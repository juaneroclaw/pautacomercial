<?php
ob_start();
session_start();
error_reporting(0);
include "header.php";
?>
        <div class="clear"></div>
        <!--//================Slider start==============//--> 
        <section id="slider-section">
            <div id="main-slider" class="owl-carousel owl-theme slider positionR">
                <div class="item positionR">
                    <figure class="slider-image positionR">
                        <img src="assets/img/slider/main-1.jpg" alt="" class="hidden-xs"/>
                        <img src="assets/img/slider/main-xs-1.jpg" alt="" class="hidden-sm hidden-lg hidden-md"/>
                    </figure>
                    <div class="slider-text positionA text-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-sm-10 col-xs-12 col-md-offset-1 col-sm-offset-1 col-xs-offset-0 text-center">
                                    <h1>Pauta<span class="theme-color">Comercial</span></h1>
                                    <p>Difusión de servicios.</p>
                                    <a href="search.php" class="itg-button light">Leer más</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item positionR">
                    <figure class="slider-image positionR">
                        <img src="assets/img/slider/main-2.jpg" alt="" class="hidden-xs"/>
                        <img src="assets/img/slider/main-xs-2.jpg" alt="" class="hidden-sm hidden-lg hidden-md"/>
                    </figure>
                    <div class="slider-text positionA text-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-sm-10 col-xs-12 col-md-offset-1 col-sm-offset-1 col-xs-offset-0 text-center">
                                    <h1>Pauta<span class="theme-color">Comercial</span></h1>
                                    <p>Difusión de servicios.</p>
                                    <a href="search.php" class="itg-button light">Leer más</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item positionR">
                    <figure class="slider-image positionR">
                        <img src="assets/img/slider/main-3.jpg" alt="" class="hidden-xs"/>
                        <img src="assets/img/slider/main-xs-3.jpg" alt="" class="hidden-sm hidden-lg hidden-md"/>
                    </figure>
                    <div class="slider-text positionA text-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-sm-10 col-xs-12 col-md-offset-1 col-sm-offset-1 col-xs-offset-0 text-center">
                                    <h1>Pauta<span class="theme-color">Comercial</span></h1>
                                    <p>Difusión de servicios.</p>
                                    <a href="search.php" class="itg-button light">Leer más</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Slider end==============//-->
        <!--//================details section Start==============//-->
        <section>
            <div class="container">
                <div class="row padT100 padB100">
                    <div class="col-md-7 col-sm-12 col-xs-12 mar-bottom-table">
                        <figure class="detail-box">
                            <img src="assets/img/all/pauta00.png" alt="">
                        </figure>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="details-section-boxs padB30">
                            <h1 class="theme-color">Revistas</h1>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="details-text-section">
                                    <h4>&nbsp;</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details-section-boxs padB30">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="service-box icons text-center">
                                        <div class="service-circle box">
                                            <span class="big-circle"><a href="#"><img src="assets/img/icon/3.png" alt=""></a></span>
                                        </div>
                                    </div>
                                    <div class="details-text-section">
                                        <h4>Revistas Digitales</h4>
                                        <p>Desarrollamos revistas digitales por Barrios.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details-section-boxs">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="service-box icons text-center">
                                        <div class="service-circle box">
                                            <span class="big-circle"><a href="#"><img src="assets/img/icon/12.png" alt=""></a></span>
                                        </div>
                                    </div>
                                    <div class="details-text-section">
                                        <h4>Catálogos Digitales</h4>
                                        <p>Desarrollamos catálogos digitales.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================details section end==============//-->
        <!--//================Services starts==============//-->
        <section class="marB100">
            <!--- Theme heading start-->
            <div class="theme-heading marB50 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                            <h1>Pauta Comercial</h1>
                            <div class="heading-lines"><span class="saf-boxs"></span></div>
                            <p>La búsqueda es mediante las siguientes opciones:</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Theme heading end-->
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 mar-bottom-res mar-bottom-table">
                        <div class="service-box hover text-center">
                            <div class="service-circle"><span class="big-circle"><img src="assets/img/icon/11.png" alt=""></span></div>
                            <h4 class="marT20 marB10"><a href="service-detail.html">Revistas</a></h4>
                            <p>Editadas por Pauta Comercial</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 mar-bottom-res mar-bottom-table">
                        <div class="service-box hover text-center">
                            <div class="service-circle"><span class="big-circle"><img src="assets/img/icon/12.png" alt=""></span></div>
                            <h4 class="marT20 marB10"><a href="service-detail.html">e-Catálogos</a></h4>
                            <p>Catálogos digitales</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 mar-bottom-res">
                        <div class="service-box hover text-center">
                            <div class="service-circle">
                                <span class="big-circle"><a href="#"><img src="assets/img/icon/5.png" alt=""></a></div>
                            <h4 class="marT20 marB10"><a href="service-detail.html">Deslizadores</a></h4>
                            <p>Muestras múltiples de imágenes</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="service-box hover text-center">
                            <div class="service-circle">
                                <span class="big-circle"><a href="#"><img src="assets/img/icon/2.png" alt=""></a></span>
                            </div>
                            <h4 class="marT20 marB10"><a href="service-detail.html">Videos</a></h4>
                            <p>Difusión a través de videos</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Services end==============//--><!--//================Banner starts==============//--><!--//================Banner end==============//-->
        <!--//================Gallery Start==============//-->
        <section class="padT100">
            <!--- Theme heading start-->
            <div class="theme-heading marB50 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                            <h1>Categorías</h1>
                            <div class="heading-lines"><span class="saf-boxs"></span></div>
                            <p>Ilimitadas catagorías de rubros
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Theme heading end-->
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 pad0">
                            <div class="gallery-img">
                                <figure>
                                    <img src="assets/img/gallery/1.jpg" alt="">
                                    <figcaption><a href="assets/img/gallery/popup-1.jpg" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 pad0">
                            <div class="gallery-img">
                                <figure>
                                    <img src="assets/img/gallery/2.jpg" alt="">
                                    <figcaption><a href="assets/img/gallery/popup-2.jpg" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 pad0">
                            <div class="gallery-img">
                                <figure>
                                    <img src="assets/img/gallery/3.jpg" alt="">
                                    <figcaption><a href="assets/img/gallery/popup-3.jpg" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 pad0">
                            <div class="gallery-img">
                                <figure>
                                    <img src="assets/img/gallery/4.jpg" alt="">
                                    <figcaption><a href="assets/img/gallery/popup-4.jpg" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 pad0">
                            <div class="gallery-img">
                                <figure>
                                    <img src="assets/img/gallery/5.jpg" alt="">
                                    <figcaption><a href="assets/img/gallery/popup-5.jpg" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 pad0">
                            <div class="gallery-img">
                                <figure>
                                    <img src="assets/img/gallery/6.jpg" alt="">
                                    <figcaption><a href="assets/img/gallery/popup-6.jpg" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Gallery end==============//-->
        <div class="clear"></div>
        <!--//================Special Menu start==============//-->
        <section class="padT100 padB100">
            <!--- Theme heading start-->
            <div class="theme-heading marB50 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                            <h1>Publicar en Pauta Comercial</h1>
                            <div class="heading-lines"><span class="saf-boxs"></span></div>
                            <p>Opciones</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Theme heading end-->
            <div class="container">
                <div class="row">
                    <div id="lunch" class="tabcontent" style="display:block;" >
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="menu-list-box marB30">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <figure>
                                            <img src="assets/img/product/14.jpg" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="col-sm-8 col-md-8">
                                        <h3><a href="service-detail.html">Revistas</a></h3>
                                        <p>Integración de revistas digitales</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="menu-list-box marB30">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <figure>
                                            <img src="assets/img/product/15.jpg" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="col-sm-8 col-md-8">
                                        <h3><a href="service-detail.html">Catálogos</a></h3>
                                        <p>
Integración de catálogos digitales</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="menu-list-box marB30">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <figure>
                                            <img src="assets/img/product/16.jpg" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="col-sm-8 col-md-8">
                                        <h3><a href="service-detail.html">Deslizadores</a></h3>
                                        <p>Integración de sliders</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="menu-list-box marB30">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <figure>
                                            <img src="assets/img/product/17.jpg" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="col-sm-8 col-md-8">
                                        <h3><a href="service-detail.html">Videos</a></h3>
                                        <p>
Integración de videos</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="menu-list-box marB30">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <figure>
                                            <img src="assets/img/product/18.jpg" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="col-sm-8 col-md-8">
                                        <h3><a href="service-detail.html">HTML</a></h3>
                                        <p>Integración de html</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="menu-list-box">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <figure>
                                            <img src="assets/img/product/21.jpg" alt=""/>
                                        </figure>
                                    </div>
                                  <div class="col-sm-8 col-md-8">
                                    <h3><a href="service-detail.html">IFrame HTML</a></h3>
                                      <p>Integración de Iframe</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Special Menu end==============//-->
        <div class="clear"></div>
        <!--//================Video start==============//-->
        <section class="opening-sec-box">
            <div class="special-style overlay">
                <div class="special-video-image parallax-style"></div>
            </div>
            <div class="padT100 padB70">
                <!--- Theme heading start-->
                <div class="theme-heading background marB50 positionR">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                                <h1>Ventajas</h1>
                                <div class="heading-lines"><span class="saf-boxs"></span></div>
                                <p>Publicando en la revista gráfica obtendrá 6 meses sin cargo&nbsp;en la web.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- Theme heading end-->
                <div class="container">
                    <div class="opening-box text-center">
                        <div class="opening-hours pad40">
                            <h2>Bonificaciones</h2>
                            <p><span><strong>50%</strong></span>  Publicando 2 avisos se obtendrá un descuento en el 2do aviso</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Video end==============//-->
        <div class="clear"></div>
        <!--//================ Our Team start ==============//-->
        <section class="padT100 padB90 team">
            <!--- Theme heading start-->
            <div class="theme-heading marB50 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                            <h1>Revistas digitales temáticas </h1>
                            <div class="heading-lines"><span class="saf-boxs"></span></div>
                            <p>Revistas por Barrio</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Theme heading end-->	
            <div class="container">
                <div class="row">
                    <div class="our-team">
                        <div id="team-slider" class="owl-carousel owl-theme slider positionR">
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/agronomia.jpg" alt=""/>
                                            <figcaption>
                                                <a href="SearchFolder/team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                        </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="members-detail grey-bg">
                                            <h4><a href="team-detail.html">Revista</a></h4>
                                            <p>Agronomía</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/almagro.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                        </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="members-detail grey-bg">
                                            <h4><a href="team-detail.html">Revista</a></h4>
                                            <p>Almagro</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/balvanera.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                        </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="members-detail grey-bg">
                                            <h4><a href="team-detail.html">Revista</a></h4>
                                            <p>Balvanera</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/barracas.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                        </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="members-detail grey-bg">
                                            <h4><a href="team-detail.html">Revista</a></h4>
                                            <p>Barracas</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/belgrano.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                        </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="members-detail grey-bg">
                                            <h4><a href="team-detail.html">Revista</a></h4>
                                            <p>Belgrano</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/boedo.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Boedo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/caballito.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Caballito</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/chacarita.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Chacarita</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/coghlan.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Coghlan</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/colegiales.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Colegiales</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/constitucion.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Constitución</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/flores.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Flores</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/floresta.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Floresta</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/laboca.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>La Boca</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/lapaternal.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>La Paternal</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/liniers.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Liniers</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/mataderos.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Mataderos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/montecastro.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Monte Castro</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/montserrat.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Monserrat </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/nuevapompeya.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Nueva Pompeya</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/nuniez.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Núñez</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/palermo.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Palermo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/parqueavellaneda.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Parque Avellaneda</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/parquechacabuco.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Parque Chacabuco</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/parquechas.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Parque Chas</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/parquepatricios.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Parque Patricios</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/puertoMadero.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Puerto Madero</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/recoleta.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Recoleta</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/retiro.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Retiro</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/saavedra.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Saavedra</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/sancristobal.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>San Cristóbal</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/sannicolas.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>San Nicolás</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/santelmo.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>San Telmo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/velezsarfield.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Vélez Sársfield</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/versalles.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Versalles</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villacrespo.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Crespo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villadelparque.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa del Parque</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villadevoto.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Devoto</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villageneralmitre.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa General Mitre</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villalugano.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Lugano</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villaluro.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Luro</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villaortuzar.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Ortúzar</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villapueyrredon.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Pueyrredón</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villareal.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Real</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villariachuelo.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Riachuelo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villasantarita.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Santa Rita</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villasoldati.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Soldati</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="team-member text-center marB30">
                                        <figure>
                                            <img src="assets/img/team/villaurquiza.jpg" alt=""/>
                                            <figcaption>
                                                <a href="team-detail.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </figcaption>
                                      </figure>
                                        <div class="product-text-sec-box">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                      <div class="members-detail grey-bg">
                                        <h4><a href="team-detail.html">Revista</a></h4>
                                          <p>Villa Urquiza</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================ Our Team end ==============//-->
        <div class="clear"></div>
        <!--//================ Our Customer start ==============//-->
        <section class="padTB100 customer-section" >
            <!--- Theme heading start-->
            <div class="theme-heading background marB100 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                            <h1> Representante de Ventas</h1>
                            <div class="heading-lines"><span class="saf-boxs"></span></div>
                            <p> Conviertase en nuestro representante de ventas y obtenga comisiones sobre la venta de publicidad tanto en la gráfica como en la web.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Theme heading end-->	
            <div class="container">
                <div class="row">
                    <div id="customer-slider" class="owl-carousel owl-theme slider positionR">
                        <div class="item">
                            <div class="col-md-12">
                                <div class="customer-box">
                                    <blockquote>
Aumente los beneficios con nuestros paquetes publicitarios para sus anunciantes.
                                    </blockquote>
                                    <div class="customer-detail">
                                        <div class="customer-img">
                                            <figure>
                                                <img src="assets/img/all/5.jpg" alt=""/>
                                            </figure>
                                        </div>
                                        <div class="caption">
                                            <h3>Paquetes publicitarios</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                                <div class="customer-box">
                                    <blockquote>
Genere mayores ingresos obteniendo anunciantes que completen ciclos completos de publicidad.
                                    </blockquote>
                                    <div class="customer-detail">
                                        <div class="customer-img">
                                            <figure>
                                                <img src="assets/img/all/6.jpg" alt=""/>
                                            </figure>
                                        </div>
                                        <div class="caption">
                                            <h3>Premios publicitarios</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                                <div class="customer-box">
                                    <blockquote>
Premie a sus anunciantes con campañas exclusivas en la web.
                                    </blockquote>
                                    <div class="customer-detail">
                                        <div class="customer-img">
                                            <figure>
                                                <img src="assets/img/all/7.jpg" alt=""/>
                                            </figure>
                                        </div>
                                        <div class="caption">
                                            <h3>Campañas en la Web</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                                <div class="customer-box">
                                    <blockquote>
Premie a sus anunciantes con campañas exclusivas en las revistas barriales.
                                  </blockquote>
                                    <div class="customer-detail">
                                        <div class="customer-img">
                                            <figure>
                                                <img src="assets/img/all/8.jpg" alt=""/>
                                            </figure>
                                        </div>
                                        <div class="caption">
                                            <h3>Campañas gráficas</h3>                                           
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p align="center">&nbsp; </p>
            <p align="center">Para convetirse en representante de ventas, contactarse a <a href="mailto:info@pautacomercial.com" title="Contacto Comercial" target="_blank">info@pautacomercial.com</a></p>
        </section>
        <!--//================ Our Customer end ==============//-->
        <div class="clear"></div>
        <!--//================Product section start==============//-->
        <section class=" padB70 padT100">
            <!--- Theme heading start-->
            <div class="theme-heading marB50 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                            <h1>Rubros:  productos / servicios</h1>
                            <div class="heading-lines"><span class="saf-boxs"></span></div>
                            <p>
Se pueden incluir ilimitadas opciones de rubros.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Theme heading end-->
            <div class="container">
                <div class="row">
                    <div id="collection-slider" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="col-md-12">
                                <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                        <img src="assets/img/product/1.jpg" alt=""/>
                                        <figcaption>
                                            <a href="SearchFolder/product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" target="_blank" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="product-detail.php" target="_blank">VINOS</a></h4>
                                    <p> Se puede optar por incluir un aviso en el portal Pauta Comercial
</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                                <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                        <img src="assets/img/product/2.jpg" alt=""/>
                                        <figcaption>
                                            <a href="product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="product-detail.html">INMUEBLES</a></h4>
                                    <p>Se puede optar por incluir un aviso en el portal Pauta Comercial </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                                <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                        <img src="assets/img/product/3.jpg" alt=""/>
                                        <figcaption>
                                            <a href="product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="product-detail.html">PRODUCTOS DE LIMPIEZA</a></h4>
                                    <p>Se puede optar por incluir un aviso en el portal Pauta Comercial. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                                <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                        <img src="assets/img/product/4.jpg" alt=""/>
                                        <figcaption>
                                            <a href="product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="product-detail.html">BIENESTAR</a></h4>
                                    <p>Se puede optar por incluir un aviso en el portal Pauta Comercial. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                                <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                        <img src="assets/img/product/5.jpg" alt=""/>
                                        <figcaption>
                                            <a href="product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="product-detail.html">REGALOS</a></h4>
                                    <p>Se puede optar por incluir un aviso en el portal Pauta Comercial. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                                <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                        <img src="assets/img/product/6.jpg" alt=""/>
                                        <figcaption>
                                            <a href="product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="product-detail.html">NOTEBOOKS-TABLETS</a></h4>
                                    <p>Se puede optar por incluir un aviso en el portal Pauta Comercial. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                              <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                      <img src="assets/img/product/7.jpg" alt=""/>
                                        <figcaption>
                                            <a href="product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                <h4 class="product-lilte-headings"><a href="product-detail.html">PROFESIONALES: ELECTRICIDAD</a></h4>
                                  <p>Se puede optar por incluir un aviso en el portal Pauta Comercial. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                              <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                      <img src="assets/img/product/8.jpg" alt=""/>
                                        <figcaption>
                                            <a href="product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                <h4 class="product-lilte-headings"><a href="product-detail.html">PROFESIONALES: PLOMERO</a></h4>
                                  <p>Se puede optar por incluir un aviso en el portal Pauta Comercial. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12">
                              <div class="collection-box product-img theme-hover sticker">
                                    <figure class="blog-style marB0">
                                        <img src="assets/img/product/9.jpg" alt=""/>
                                        <figcaption>
                                            <a href="product-detail.php"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="product-detail.php" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                <li><a href="product-detail.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="product-detail.php" class="itg-button light">Ver aviso</a>
                                        </div>
                                    </div>
                                <h4 class="product-lilte-headings"><a href="product-detail.html">PROFESIONALES: PINTORES</a></h4>
                                  <p>Se puede optar por incluir un aviso en el portal Pauta Comercial. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================product section end==============//-->
        <div class="clear"></div>
        <!--//================price table start==============//--><!--//================price table end==============//-->
<div class="clear"></div>
        <!--//================ Blog start ==============//-->
        <section class="padB70">
            <!--- Theme heading start-->
            <div class="theme-heading marB50 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                            <h1>Destacados</h1>
                            <div class="heading-lines"><span class="saf-boxs"></span></div>
                            <p>
Dentro del portal Pauta Comercial hay opciones de incluir publicaciones destacadas.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Theme heading end-->	
            <div class="container">
                <div class="row">
                    <div id="blog-slider" class="owl-carousel owl-theme slider positionR">
                        <div class="item">
                            <div class="col-md-12 box-shadow-hover">
                                <div class="collection-box product-img hover theme-hover marB30">
                                    <figure class="blog-style">
                                        <img src="assets/img/blog/1.jpg" alt=""/>
                                        <figcaption>
                                            <a href="http://www.claudiaberlusconi.com" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="http://www.claudiaberlusconi.com"><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                                <li><a href="http://www.claudiaberlusconi.com"><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="http://www.claudiaberlusconi.com" target="_blank" class="itg-button light">Ver +</a></div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="http://www.claudiaberlusconi.com" target="_blank">Claudia Berlusconi</a></h4>
                                    <p> Fileteadora porteña - Artista Plástica
</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="item">
                            <div class="col-md-12 box-shadow-hover">
                                <div class="collection-box product-img hover theme-hover marB30">
                                    <figure class="blog-style">
                                        <img src="assets/img/blog/2.jpg" alt=""/>
                                        <figcaption>
                                            <a href="http://www.baenjoyit.com" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                  </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="http://www.baenjoyit.com"><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                                <li><a href="http://www.baenjoyit.com"><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="http://www.baenjoyit.com" target="_blank" class="itg-button light">Ver +</a>
                                        </div>
                                    </div>
                                  <h3 class="product-lilte-headings"><a href="http://www.baenjoyit.com" target="_blank">BA Enjoyit </a></h3>
                                    <p>Portal de Turismo de Buenos Aires</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12 box-shadow-hover">
                                <div class="collection-box product-img hover theme-hover marB30">
                                    <figure class="blog-style">
                                        <img src="assets/img/blog/3.jpg" alt=""/>
                                        <figcaption>
                                            <a href="https://correctorasdepapel.blogspot.com" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="https://correctorasdepapel.blogspot.com"><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                                <li><a href="https://correctorasdepapel.blogspot.com"><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="https://correctorasdepapel.blogspot.com" target="_blank" class="itg-button light">Ver +</a>
                                        </div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="https://correctorasdepapel.blogspot.com" target="_blank">Correctora de Papel</a></h4>
                                    <p>Stella Maris Roque. - Correctora profesional - Periodista</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12 box-shadow-hover">
                                <div class="collection-box product-img hover theme-hover marB30">
                                    <figure class="blog-style">
                                        <img src="assets/img/blog/4.jpg" alt=""/>
                                        <figcaption>
                                            <a href="https://www.facebook.com/vinoamicopa" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="https://www.facebook.com/vinoamicopa"><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                                <li><a href="https://www.facebook.com/vinoamicopa"><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="https://www.facebook.com/vinoamicopa" target="_blank" class="itg-button light">Ver +</a>
                                        </div>
                                    </div>
                                    <h4 class="product-lilte-headings"><a href="https://www.facebook.com/vinoamicopa" target="_blank">Vino a mi copa </a>&nbsp;</h4>
                                    <p>Degustación de vinos</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12 box-shadow-hover">
                                <div class="collection-box product-img hover theme-hover marB30">
                                    <figure class="blog-style">
                                        <img src="assets/img/blog/5.jpg" alt=""/>
                                        <figcaption>
                                            <a href="https://akitsuconsulting.wordpress.com/acerca-de" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                    </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="https://akitsuconsulting.wordpress.com/acerca-de"><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                                <li><a href="https://akitsuconsulting.wordpress.com/acerca-de"><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="https://akitsuconsulting.wordpress.com/acerca-de" target="_blank" class="itg-button light">Ver +</a>
                                        </div>
                                    </div>
                                  <h3 class="product-lilte-headings"><a href="https://akitsuconsulting.wordpress.com/acerca-de/" target="_blank">Akitsu</a><a href="blog-full-with-sidebar.html"></a></h3>
                                    <p>Consultora de Biodescodificación – BioNeuroEmoción – Lógica Global Convergente</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-12 box-shadow-hover">
                                <div class="collection-box product-img hover theme-hover marB30">
                                    <figure class="blog-style">
                                        <img src="assets/img/blog/6.jpg" alt=""/>
                                        <figcaption>
                                            <a href="https://disfrutemosba.buenosaires.gob.ar" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </figcaption>
                                  </figure>
                                    <div class="product-text-sec-box">
                                        <div class="product-text-sec-icons1">
                                            <ul>
                                                <li><a href="https://disfrutemosba.buenosaires.gob.ar"><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                                <li><a href="https://disfrutemosba.buenosaires.gob.ar"><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                            </ul>
                                        </div>
                                        <div class="product-text-sec-btn1">
                                            <a href="https://disfrutemosba.buenosaires.gob.ar" target="_blank" class="itg-button light">Ver +</a>
                                        </div>
                                    </div>
                                  <h3 class="product-lilte-headings"><a href="https://disfrutemosba.buenosaires.gob.ar" target="_blank">DisfrutemosBA</a><a href="blog-full-with-sidebar.html"></a></h3>
                                    <p>Siempre hay algo para hacer en al Ciudad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================ Blog end ==============//-->
        <div class="clear"></div>
        <!--//================ Our Partner start ==============//-->
        <div class="partner-box">
            <div class="container">
                <div class="row">
                    <div id="partner-slider" class="owl-carousel owl-theme owl-loaded owl-drag">
                        <div class="col-xs-12">
                            <div class="item">
                                <figure>
                                    <img src="assets/img/partner/4.png" alt="">
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="item">
                                <figure>
                                    <img src="assets/img/partner/5.png" alt="">
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="item">
                                <figure>
                                    <img src="assets/img/partner/6.png" alt="">
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="item">
                                <figure>
                                    <img src="assets/img/partner/7.png" alt="">
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="item">
                                <figure>
                                    <img src="assets/img/partner/8.png" alt="">
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="item">
                                <figure>
                                    <img src="assets/img/partner/6.png" alt="">
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="item">
                                <figure>
                                    <img src="assets/img/partner/7.png" alt="">
                                </figure>
                            </div> 
                        </div>
                        <div class="col-xs-12">
                            <div class="item">
                                <figure> 
                                    <img src="assets/img/partner/8.png" alt="">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//================ Our Partner end ==============//-->
        <div class="clear"></div>
        <?php
		include "footer.php";
		?>
        