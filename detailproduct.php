<?php
$check=0;
session_start();
error_reporting(0);
//Include database configuration file
include('config.php');
if(isset($_GET['id'])){
  $id = $_GET['id'];
  $sql = "select * from `products` where `product_id`='$id'";
  $result = mysqli_query($con,$sql);
  $rowinfo = mysqli_num_rows($result);
  $row = mysqli_fetch_array($result);
}
?>
<link href="css/sweet.css" rel="stylesheet" >
<link href="css/style.css" rel="stylesheet" >
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php
include "header.php";
?>
	<div class="clear"></div>
	<!--//================Filter start==============//-->
	<section class="">
    <div class="filter-section box padT100 padB100">
			
			<div id="mainContainer" class="container">
			

			         <div class="Quick-view-popup modal-content text-left">
                            <div class="modal-header">
							 <h3 class="theme-headdings text-left  product-detail-title s"><a href="#">Pauta Products Details</a></h3>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="col-md-12 col-xs-12 popap-open-box">
                                <div class="row">
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-12 marB30">
                                                <figure>
												
												
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
       <a href="product-detail.php?file=<?php echo $row['products_img'];?>"> <img src="<?php echo $row['products_img']; ?>" alt="My Image" style="width:350px; height:450px;"></a>
        <div class="carousel-caption">
          <h3>My Image</h3>
          <p>Pauta Images!</p>
        </div>
      </div>

      <div class="item">
         <img src="<?php echo $row['products_file']; ?>" alt="My PDF" style="width:350px; height:450px;">
        <div class="carousel-caption">
          <h3>My PDF</h3>
          <p>Pauta PDF!</p>
        </div>
      </div>
    
      <div class="item">
        <img src="<?php echo $row['embedded_imgurl']; ?>" alt="My Video" style="width:350px; height:450px;">
        <div class="carousel-caption">
          <h3>My Video</h3>
          <p>Pauta Videos!</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>							
     
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12 responsive-top">
                                        <h3 class="theme-headdings text-left  product-detail-title s"><a href="#"><?php echo $row['products_name']; ?></a></h3>
                                        <div class="clear"></div>
                                        <div class="per-box texy-left marT15">
                                            <p>
                                            <?php echo $row['products_des']; ?></p>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
			
			
	        </div>
	</div>
	</section>
	
	<!--//================Related Products end==============//-->
	<div class="clear"></div>
<?php
include "footer.php";
?>