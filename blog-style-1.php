<?php
include "header.php";
?>
        <div class="clear"></div>
        <!--//================Bredcrumb starts==============//-->
        <section>
            <div class="bredcrumb-section padTB100 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-head">
                                <div class="page-header-heading">
                                    <h3 class="theme-color">Blog</h3>
                                </div>
                                <div class="breadcrumb-box">
                                    <ul class="breadcrumb colorW marB0">
                                        <li>
                                            <a href="index.html">Casa</a>
                                        </li>
                                        <li class="active">Blog</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Bredcrumb end==============//-->
        <div class="clear"></div>
        <!--//================Blog start==============//-->
        <section class="padT100 padB70">
            <div class="container">
                <div class="row marB20 ">
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/1.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/2.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/3.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/4.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/3.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/5.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/1.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/6.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                        <div class="collection-box product-img hover theme-hover marB30">
                            <figure class="blog-style">
                                <img src="assets/img/blog/2.jpg" alt=""/>
                                <figcaption>
                                    <a href="blog-full-with-sidebar.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </figcaption>
                            </figure>
                            <div class="product-text-sec-box">
                                <div class="product-text-sec-icons1">
                                    <ul>
                                        <li><a href=""><i class="fa fa-heart" aria-hidden="true"></i>30</a></li>
                                        <li><a href=""><i class="fa fa-comment" aria-hidden="true"></i>30</a></li>
                                    </ul>
                                </div>
                                <div class="product-text-sec-btn1">
                                    <a href="blog-full-with-sidebar.html" class="itg-button light">Lee mas</a>
                                </div>
                            </div>
                            <h4 class="product-lilte-headings"><a href="blog-full-with-sidebar.html">Tu texto aqui</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the rinting and typesetting industry. Lorem Ipsum has been the industry's</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="pagination-box text-right marB30">
                            <a href="#"><span><i class="fa fa-angle-left" aria-hidden="true"></i></span></a>
                            <a href="#"><span>1</span></a>
                            <a href="#"><span>2</span></a>
                            <a href="#"><span>3</span></a>
                            <a href="#"><span>4</span></a>
                            <a href="#"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Blog end==============//-->
        <div class="clear"></div>
<?php
include "footer.php";
?>