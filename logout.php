  <?php
  session_start();
  if(isset($_SESSION['user_id'])) {
	  session_destroy();
	  unset($_SESSION['user_id']);
	  unset($_SESSION['user_email']);
		unset($_SESSION['first_name']);
		unset($_SESSION['last_name']);
		unset($_SESSION['user_date']);
		unset($_SESSION['user_country']);
		unset($_SESSION['user_province']);
		unset($_SESSION['user_city']);
		unset($_SESSION['user_neighbourhood']);
	  ?>
	<script>
	
	window.location.href="AdminLogin.php";
	</script>
	<?php
  } else {
	  ?>
	<script>
	
	window.location.href="AdminLogin.php";
	</script>
	<?php
  }
  ?>