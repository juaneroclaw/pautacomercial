<?php
    session_start();
    include_once 'config.php';
    $num_rec_per_page=7;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
    $start_from = ($page-1) * $num_rec_per_page;
    $sql = "select * from products LIMIT $start_from, $num_rec_per_page";
    $result = mysqli_query($con,$sql);
    ?>
<!DOCTYPE html>
<html lang="zxx">
    <!--
        **********************************************************************************************************
        Copyright (c) 2017 It-Geeks
        ********************************************************************************************************** 
        -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
        <![endif]-->
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Responsive html template for Salon and Spa" />
        <meta name="author" content="itgeeksin.com" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Responsive html template for Salon and Spa</title>
        <!-- Bootstrap -->
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
        <!-- Master Css -->
        <link href="main.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		
		<style>
            .table th {
                text-align:center;
                display: table-cell;
                vertical-align: inherit;
                font-weight: bold;
                background-color: black;
            }
            .bs-example{
            margin: 20px;
        }
        </style>
		<script src="jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#category').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'category_id='+countryID,
                success:function(html){
                    $('#subcategory').html(html);
                    $('#products').html('<option value="">Select subcategory first</option>'); 
                }
            }); 
        }else{
            $('#subcategory').html('<option value="">Select category first</option>');
            $('#products').html('<option value="">Select subcategory first</option>'); 
        }
    });
    

});
</script>
    </head>
    <body>
        <!--//================preloader  start==============//--> 
        <div class="preloader">
            <div id="loading-center">
                 <img class="logo-nav" alt="" src="assets/img/productcatalog.png">
                <div class="loader"></div>
            </div>
        </div>
        <!--//================preloader  end==============//-->  
        <!--//================Header start==============//-->  
        <header class="positionR">
            <div id="main-menu" class="wa-main-menu">
                <!-- Menu -->
                <div class="wathemes-menu relative">
                    <!-- navbar -->
                    <div class="navbar navbar-default black-bg mar0" role="navigation">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    <div class="row">
                                        <div class="navbar-header">
                                            <!-- Button For Responsive toggle -->
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="sr-only">Toggle navigation</span> 
                                            <span class="icon-bar"></span> 
                                            <span class="icon-bar"></span> 
                                            <span class="icon-bar"></span>
                                            </button> 
                                            <!-- Logo -->
                                            <a class="navbar-brand box" href="index.html">
                                            <img class="logo-nav" alt="" src="assets/img/logo.png" />
                                            </a>
                                        </div>
                                        <!-- Navbar Collapse -->
                                        <div class="navbar-collapse collapse">
                                            <!-- nav -->
                                            <ul class="nav navbar-nav">
                                                <li><a href="index.html">home</a></li>
                                                <li>
                                                    <a href="#">pages<i class="fa fa-angle-down" aria-hidden="true"></i></a> 
                                                    <ul class="dropdown-menu">
                                                        <li><a href="booking.html">booking</a></li>
                                                        <li>
                                                            <a href="#">about
                                                            <i class="fa fa-angle-right hidden-xs" aria-hidden="true"></i>
                                                            <i class="fa fa-angle-down hidden-md hidden-sm hidden-lg" aria-hidden="true"></i>
                                                            </a>
                                                            <ul class="dropdown-menu left-side menu">
                                                                <li><a href="about-us.html">about us</a></li>
                                                                <li><a href="team.html">Team</a></li>
                                                                <li><a href="team-detail.html">Team Detail</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="left-side">
                                                            <a href="#">gallery
                                                            <i class="fa fa-angle-right hidden-xs" aria-hidden="true"></i>
                                                            <i class="fa fa-angle-down hidden-md hidden-sm hidden-lg" aria-hidden="true"></i></a>
                                                            <ul class="dropdown-menu gallery-dropdown-left">
                                                                <li><a href="gallery-style-1.html">style 1</a></li>
                                                                <li><a href="gallery-style-2.html">style 2</a></li>
                                                                <li><a href="gallery-style-3.html">style 3</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="left-side">
                                                            <a href="#">comming soon
                                                            <i class="fa fa-angle-right hidden-xs" aria-hidden="true"></i>
                                                            <i class="fa fa-angle-down hidden-md hidden-sm hidden-lg" aria-hidden="true"></i></a>
                                                            <ul class="dropdown-menu gallery-dropdown-left">
                                                                <li><a href="comming-soon-1.html">comming soon 1</a></li>
                                                                <li><a href="comming-soon-2.html">comming soon 2</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="faq.html">FAQ</a></li>
                                                        <li><a href="user-profile.html">user</a></li>
                                                        <li><a href="register.html">register</a></li>
                                                        <li><a href="404-error.html">404 error</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">services<i class="fa fa-angle-down" aria-hidden="true"></i></a> 
                                                    <ul class="dropdown-menu">
                                                        <li><a href="service.html">services</a></li>
                                                        <li><a href="service-2.html">service 2</a></li>
                                                        <li><a href="service-detail.html">service detail</a></li>
                                                        <li><a href="special-service.html">Special Service</a></li>
                                                        <li><a href="search.html">search</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Shop<i class="fa fa-angle-down" aria-hidden="true"></i></a> 
                                                    <ul class="dropdown-menu">
                                                        <li><a href="collection-fullwidth.html">Collection Full</a></li>
                                                        <li><a href="collection-list-sidebar.html">Collection Sidebar</a></li>
                                                        <li><a href="cart.html">Cart</a></li>
                                                        <li><a href="checkout.html">Checkout</a></li>
                                                        <li><a href="order.html">Order</a></li>
                                                        <li><a href="product-detail.html">Product Detail</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">blog<i class="fa fa-angle-down" aria-hidden="true"></i></a> 
                                                    <ul class="dropdown-menu">
                                                        <li><a href="blog-style-1.html">Blogs Style 1</a></li>
                                                        <li><a href="blog-style-2.html">Blogs Style 2</a></li>
                                                        <li><a href="blog-style-3.html">Blogs Style 3</a></li>
                                                        <li><a href="blog-style-4.html">Blogs Style 4</a></li>
                                                        <li><a href="blog-style-5.html">Blogs Style 5</a></li>
                                                        <li>
                                                            <a href="#" class="menu-left-side">Single Blog 
                                                            <i class="fa fa-angle-right hidden-xs" aria-hidden="true"></i>
                                                            <i class="fa fa-angle-down hidden-md hidden-sm hidden-lg" aria-hidden="true"></i></a>
                                                            <ul class="dropdown-menu left-side menu">
                                                                <li><a href="blog-full-with-sidebar.html">With left Sidebar</a></li>
                                                                <li><a href="blog-full-with-right-sidebar.html">With right Sidebar</a></li>
                                                                <li><a href="blog-full.html">Without Sidebar</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">contact us<i class="fa fa-angle-down" aria-hidden="true"></i></a> 
                                                    <ul class="dropdown-menu">
                                                        <li><a href="contact-us.html">contact us style 1</a></li>
                                                        <li><a href="contact-us-style-2.html">contact us style 2</a></li>
                                                    </ul>
                                                </li>
												<li class="left-side">
                                                            <a href="#">Admin Panel
                                                            <i class="fa fa-angle-right hidden-xs" aria-hidden="true"></i>
                                                            <i class="fa fa-angle-down hidden-md hidden-sm hidden-lg" aria-hidden="true"></i></a>
                                                            <ul class="dropdown-menu gallery-dropdown-left">
                                                                <li><a href="AddServices.php">Add Services</a></li>
                                                                <li><a href="viewdetails.php">View Services</a></li>
                                                                <li><a href="viewusers.php">Edit Users</a></li>
                                                            </ul>
                                                        </li>
                                            </ul>
                                        </div>
                                        <!-- navbar-collapse -->
                                    </div>
                                </div>
                                <!-- col-md-12 -->
                                <!-- col-md-3 -->
                                <div class="col-md-4 col-sm-4">
                                    <div class="nav-seach-box">
                                        <input type="text" name="search" class="search_terms" placeholder="Search">
                                        <button class="button-style" type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="navigation-icon custom-drop">
                                        <ul class="social-icon top-bar-icon">
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- col-md-3 -->
                            </div>
                            <!-- row -->
                        </div>
                        <!-- container -->
                    </div>
                    <!-- navbar -->
                </div>
                <!--  Menu -->
            </div>
        </header>
        <!--//================Header end==============//-->
        <div class="clear"></div>
        <!--//================Bredcrumb starts==============//-->
        <section>
            <div class="bredcrumb-section padTB100 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-head">                              
								<div class="page-header-heading">
                                <p style="padding-left:550px;"    class="text-danger">Admin Panel  </p>
                                <span style="padding-left:480px;" class="text-info">List Of Services On The Solan Portal</span>
                            </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Bredcrumb end==============//-->
        <div class="clear"></div>
        <!--//================Register start==============//-->
		
		
		
		<div id="mainContainer" class="container" >
                <div class="shadowBox">
                    <div class="page-container">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 ">
                                    <div style="padding-top:60px;" class="table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th>Product Id</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Product Image</th>
                <th>Product File</th>
				<th>Product Status</th>
				<th>Delete</th>
				<th>Update</th>
            </tr>
        </thead>
        <tbody>
          <?php while ($row = mysqli_fetch_array($result)) { ?>
              <tr>
                <td><?php echo $row[0]; ?></td>
                <td><?php echo $row[1]; ?></td>
                <td><?php echo $row[2]; ?></td>
                <td><?php echo $row[3]; ?></td>
                <td><?php echo $row[4]; ?></td>
				<td><?php echo $row[6]; ?></td>
				<td><a href="DeleteProducts.php?id=<?php echo $row[0];?>"onClick="return confirm('Delete this Service')">Delete</a></td>
 <td> <a href="UpdateProducts.php?id=<?php echo $row[0];?>">Update</a></td>
              </tr>
          <?php } ?>
          
        </tbody>
    </table>
    <?php 
  $sql = "SELECT * FROM products"; 
  $rs_result = mysqli_query($con,$sql); //run the query
  $total_records = mysqli_num_rows($rs_result);  //count number of records
  $total_pages = ceil($total_records / $num_rec_per_page); 
  
  echo "<a href='viewdetails.php?page=1'>".'|<'."</a> "; // Goto 1st page  
  
  for ($i=1; $i<=$total_pages; $i++) { 
              echo "<a href='viewdetails.php?page=".$i."'>".$i."</a> "; 
  }; 
  echo "<a href='viewdetails.php?page=$total_pages'>".'>|'."</a> "; // Goto last page
  ?>
     </div>
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--//================Register end==============//-->
        <div class="clear"></div>
        <!--//================Footer start==============//-->
        <footer class="main_footer">
            <div class="container">
                <div class="footer-box padT50 padB30">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">About us</h3>
                                <p> 
                                    Lorem Ipsum is simply dummy text of the simply dummy printing typese tting
                                    and industry
                                </p>
                                <ul class="footer-icon-box">
                                    <li> 
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <span>hello@gmail.com</span>
                                    </li>
                                    <li> 
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <span>P: 3333 222 1111</span>
                                    </li>
                                    <li> 
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span> 99 Barnard St States - GA 22222 </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">Customer care</h3>
                                <ul class="pad0">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Customer Service</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Site Map</a></li>
                                    <li><a href="#">Advanced Search</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">Your account</h3>
                                <ul class="pad0">
                                    <li><a href="#">Sign In</a></li>
                                    <li><a href="#">View Cart</a></li>
                                    <li><a href="#">My Wishlist</a></li>
                                    <li><a href="#">Track My Order</a></li>
                                    <li><a href="#">Forum Support</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec foot-img-box marB30">
                                <h3 class="colorW marB20">Gallery</h3>
                                <ul class="pad0">
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(1).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(2).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(3).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(4).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(5).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(6).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(7).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(8).jpg" alt=""/></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new-letter-section marT50 marB50">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-12 new-letter-box-text">
                            <h3>Our Newsletter</h3>
                            <p>Contrary to popular belief, Lorem Ipsum</p>
                        </div>
                        <div class="col-md-4 col-sm-4 col-12">
                            <div class="nav-seach-box">
                                <input type="text" name="search" class="search_terms" placeholder="email@example.com">
                                <button class="button-style" type="submit" value=""><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="footer-bottom-icons-section">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="foot-sec box1">
                                        <ul class="social-icon">
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-footer padTB20 bagB">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 colorW">
                            <p><a href="http://www.itgeeksin.com/"><span class="theme-color">© IT GEEKS</span></a></p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 colorW">
                            <ul class="bottom-footer-navigation">
                                <li><a href="index.html">Home</a></li>
                                <li><a href="">Pages</a></li>
                                <li><a href="service.html">Services</a></li>
                                <li><a href="gallery-style-1.html">Portfolio</a></li>
                                <li><a href="blog-full-with-sidebar.html">Blog</a></li>
                                <li><a href="contact-us-style-2.html">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--//================Footer end==============//-->	
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/plugin/megamenu/js/hover-dropdown-menu.js"></script>
        <script src="assets/plugin/megamenu/js/jquery.hover-dropdown-menu-addon.js"></script>
        <script src="assets/plugin/owl-carousel/js/owl.carousel.min.js"></script>
        <script src="assets/plugin/acordian/js/jquery-ui.js"></script>
        <script src="assets/plugin/fancyBox/js/jquery.fancybox.pack.js"></script> 		
        <script src="assets/plugin/fancyBox/js/jquery.fancybox-media.js"></script>
        <script src="assets/plugin/vertical-slider/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="assets/plugin/counter/js/jquery.countTo.js"></script> 
        <script type="text/javascript" src="assets/plugin/counter/js/jquery.appear.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>