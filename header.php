<?php

session_start();

?>

<!DOCTYPE html>

<html lang="zxx">

      <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--[if IE]>

        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>

        <![endif]-->

        <meta name="keywords" content="HTML5 Template" />

        <meta name="description" content="Guia de productos y servicios" />

        <meta name="author" content="Pauta Comercial" />

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Pauta Comercial</title>

        <!-- Bootstrap --

        <!-- Favicon -->

        <link rel="shortcut icon" href="assets/img/pautaAvatar.ico" type="image/x-icon">

        <link rel="icon" href="assets/img/pautaAvatar.ico" type="image/x-icon">

        <!-- Master Css -->

        <link href="main.css" rel="stylesheet">

		<link href="css/sweet.css" rel="stylesheet" >

        <link href="css/style.css" rel="stylesheet" >

		
    </head>

  <body>

        <!--//================preloader  start==============//--> 

        <div class="preloader">

            <div id="loading-center">

                <img class="logo-nav" alt=""  src="assets/img/pautacomercial.png" height="300" width="300">

                <div class="loader"></div>

            </div>

        </div>

        <!--//================preloader  end==============//-->  

        <!--//================Header start==============//-->  

        <header class="positionR">

            <div id="main-menu" class="wa-main-menu">

                <!-- Menu -->

                <div class="wathemes-menu relative">

                    <!-- navbar -->

                    <div class="navbar navbar-default black-bg mar0" role="navigation">

                        <div class="container">

                            <div class="row">

                                

									<div class="col-md-12 col-sm-12">

									<div class="col-md-4 col-sm-4">

                                        <div class="navbar-header">

                                            <!-- Button For Responsive toggle -->

                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                                            <span class="sr-only">Toggle navigation</span> 

                                            <span class="icon-bar"></span> 

                                            <span class="icon-bar"></span> 

                                            <span class="icon-bar"></span>

                                            </button> 

                                            <!-- Logo -->

                                            <a class="navbar-brand box" href="index.php">

                                            <img class="logo-nav" alt="" style="height:163px;width:200px" src="assets/img/pauta2020.png" />

                                            </a>

                                        </div>

										</div>

									<div class="col-md-8 col-sm-8" style="padding-left:400px;">

								    <div class="nav-seach-box">

                                        <input type="text" name="search" class="search_terms" placeholder="Buscar">

                                        <button class="button-style" type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>

                                    </div>

									<style>

									select {

                                            font-family: Calbari;

                                            font-size: 18px;

											height: 36px;important!

											}

									</style>
							
                                   

								  </div>

                                </div>

									<div class="col-md-12 col-sm-12" style="padding-left:40px;">

                                    <div class="row">

                                        <!-- Navbar Collapse -->

                                        <div class="navbar-collapse collapse" >

                                            <!-- nav -->

										<?php  

										if (isset($_SESSION['user_email'])=="superadmin@gmail.com") { ?>

                                        <ul class="nav navbar-nav" style="float:left;">

                                                <li><a href="index.php">Inicio</a></li>

												<li><a href="about-us.php">Sobre nosotros</a></li> 

                                                <li><a href="medios.php">Buscar</a></li>

                                                <li><a href="blog-style-1.php">Blogs</a></li>

                                                <li><a href="contact-us.php">Contáctenos</a></li>

												<li><a href="#">Productos<i class="fa fa-angle-down" aria-hidden="true"></i></a> 

                                                    <ul class="dropdown-menu">

                                                        <li><a href="medios.php">Buscar</a></li>

                                                        <li><a href="product-detail.php">Detalle del producto</a></li>

                                                    </ul>

                                                </li>

												

												

												<?php if (isset($_SESSION['user_id'])) { ?>

												<li><a href="#">Funciones administrativas<i class="fa fa-angle-down" aria-hidden="true"></i></a> 

                                                    <ul class="dropdown-menu">

                                                        <li><a href="Allusers.php">Lista de usuarios</a></li>

														<li><a href="AddProduct.php">Agregar productos</a></li>

														<li><a href="AddSubcategories.php">Agregar Subcategoría</a></li>

                                                    </ul>

                                                </li>

												<li><a href="#"><?php echo $_SESSION['user_email']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>

												<ul class="dropdown-menu">

                                                         <li><a href="logout.php"><span>Cerrar sesión</span></a></li>

                                                    </ul>

												</li>

												<?php } else { ?>

										        <li><a href="login.php">Iniciar sesión</a></li>

										       

										        <?php } ?>

							                    </ul>

										        <?php } else { ?>

                                                <ul class="nav navbar-nav" style="float:left;">

                                                <li><a href="index.php">Inicio</a></li>

												<li><a href="about-us.php">Sobre nosotros</a></li> 

                                                <li><a href="medios.php">Buscar</a></li>

                                                <li><a href="contact-us.php">Contáctenos</a></li>

                                                												

												</ul>

										<?php } ?>

                                        </div>

                                        <!-- navbar-collapse -->

                                    </div>

                                </div>

                                <!-- col-md-12 -->

                                <!-- col-md-3 -->

                                

                                <!-- col-md-3 -->

                            </div>

                            <!-- row -->

                        </div>

                        <!-- container -->

                    </div>

                    <!-- navbar -->

                </div>

                <!--  Menu -->

            </div>

        </header>

        <!--//================Header end==============//-->

		

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <script src="assets/js/jquery.min.js"></script>

        <script src="assets/js/bootstrap.min.js"></script>

        <script src="assets/plugin/megamenu/js/hover-dropdown-menu.js"></script>

        <script src="assets/plugin/megamenu/js/jquery.hover-dropdown-menu-addon.js"></script>

        <script src="assets/plugin/owl-carousel/js/owl.carousel.min.js"></script>

        <script src="assets/plugin/acordian/js/jquery-ui.js"></script>

        <script src="assets/plugin/fancyBox/js/jquery.fancybox.pack.js"></script> 		

        <script src="assets/plugin/fancyBox/js/jquery.fancybox-media.js"></script>

        <script src="assets/plugin/vertical-slider/js/jquery.bxslider.js"></script>

        <script type="text/javascript" src="assets/plugin/counter/js/jquery.countTo.js"></script> 

        <script type="text/javascript" src="assets/plugin/counter/js/jquery.appear.js"></script>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnJzryw5UTYs4q-8UN4FyTwWIHlxMSYXw"></script>

        <script src="assets/js/main.js"></script>

    </body>

</html>