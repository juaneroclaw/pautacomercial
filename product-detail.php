	<?php

	    include('header.php');

	    include('config.php');

		$productid = $_GET["id"];

		$query="select * from products where product_id='$productid'";

		$result=mysqli_query($con,$query);

		$row=mysqli_fetch_array($result);

		?>



        <div class="clear"></div>

        <!--//================Bredcrumb starts==============//-->

         <section>

            <div class="bredcrumb-section padTB100 positionR">

                <div class="container">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="page-head">

                                <div class="page-header-heading">

                                    <h1 style="padding-left:450px;" class="theme-color">Detalles del producto y/o servicio</h1>

									

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <!--//================Bredcrumb end==============//-->

        <div class="clear"></div>

        <!--//================order page Start==============//-->

        <section class="order-page">

            <div class="container">

                <div class="row padT100 padB100">

                    <div class="col-md-5 col-sm-5 col-xs-12">

                        <div class="">

                            <div class="col-md-10 col-sm-10 col-xs-12 pull-right ">

                                <figure>

                                    <a href="assets/img/all/product-detail1.jpg" class="fancybox" data-fancybox-group="group">

									 <img style="min-height: 250px;" width="250" height="250" src="<?php echo $row['products_img']; ?>" alt="">

                                    </a>

                                </figure>

                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-2 popap-open hidden-xs">

                                <div class="row responsive-top vertical-slider-box padT50">

                                    <div class="slider8">

                                        <div class="col-md-12 col-sm-12 col-xs-12 padT30 popap-box slide">

                                            <figure>

                                                <a href="assets/img/all/product-detail2.jpg" class="fancybox" data-fancybox-group="group">

                                                <img src="assets/img/all/product-img2.jpg" alt="">

                                                </a>

                                            </figure>

                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12 padT30 popap-box slide">

                                            <figure>

                                                <a href="assets/img/all/product-detail3.jpg" class="fancybox" data-fancybox-group="group">

                                                <img src="assets/img/all/product-img3.jpg" alt="">

                                                </a>

                                            </figure>

                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12 padT30 popap-box pro-box-1 slide">

                                            <figure>

                                                <a href="assets/img/all/product-detail1.jpg" class="fancybox" data-fancybox-group="group">

                                                <img src="assets/img/all/product-img1.jpg" alt="">

                                                </a>

                                            </figure>

                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12 padT30 popap-box slide">

                                            <figure>

                                                <a href="assets/img/all/product-detail2.jpg" class="fancybox" data-fancybox-group="group">

                                                <img src="assets/img/all/product-img2.jpg" alt="">

                                                </a>

                                            </figure>

                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12 padT30 popap-box pro-box-1 slide">

                                            <figure>

                                                <a href="assets/img/all/product-detail3.jpg" class="fancybox" data-fancybox-group="group">

                                                <img src="assets/img/all/product-img3.jpg" alt="">

                                                </a>

                                            </figure>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 hidden-lg hidden-md hidden-sm">

                                <div class="vertical-slider-2 padT30">

                                    <div class="slider10">

                                        <div class="slide">

                                            <a href="assets/img/all/product-detail1.jpg" class="fancybox" data-fancybox-group="group">

                                            <img src="assets/img/all/product-img1.jpg" alt="">

                                            </a>

                                        </div>

                                        <div class="slide">

                                            <a href="assets/img/all/product-detail2.jpg" class="fancybox" data-fancybox-group="group">

                                            <img src="assets/img/all/product-img2.jpg" alt="">

                                            </a>

                                        </div>

                                        <div class="slide">

                                            <a href="assets/img/all/product-detail3.jpg" class="fancybox" data-fancybox-group="group">

                                            <img src="assets/img/all/product-img3.jpg" alt="">

                                            </a>

                                        </div>

                                        <div class="slide">

                                            <a href="assets/img/all/product-detail1.jpg" class="fancybox" data-fancybox-group="group">

                                            <img src="assets/img/all/product-img1.jpg" alt="">

                                            </a>

                                        </div>

                                        <div class="slide">

                                            <a href="assets/img/all/product-detail2.jpg" class="fancybox" data-fancybox-group="group">

                                            <img src="assets/img/all/product-img2.jpg" alt="">

                                            </a>

                                        </div>

                                        <div class="slide">

                                            <a href="assets/img/all/product-detail3.jpg" class="fancybox" data-fancybox-group="group">

                                            <img src="assets/img/all/product-img3.jpg" alt="">

                                            </a>

                                        </div>

                                        <div class="slide">

                                            <a href="assets/img/all/product-detail1.jpg" class="fancybox" data-fancybox-group="group">

                                            <img src="assets/img/all/product-img1.jpg" alt="">

                                            </a>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-7 col-sm-7 col-xs-12 responsive-top">

                        <h3 class="theme-headdings text-left  product-detail-title s"><a href="#"><?php echo ($row['products_name']);?></a></h3>

                        <div class="star-box-section left product-det text-left padT15">

                            <ul>

                                <li>

                                    <p>	<i class="fa fa-star"></i>

                                        <i class="fa fa-star"></i>

                                        <i class="fa fa-star"></i>

                                        <i class="fa fa-star"></i>

                                        <i class="star-code fa fa-star-half-o" aria-hidden="true"></i>

                                    </p>

                                </li>

                                <li class="border-left-site">(3) Comentarios</li>

                                <li class="border-left-site">Agrega una reseña</li>

                            </ul>

                        </div>

                        <div class="clear"></div>

                        <div class="per-box texy-left marT15">

                            <p><a href="#"><?php echo ($row['products_des']);?></a></p>

                        </div>

                        <div class="head-medial-text padT10">

                            <p>Disponibilidad: <span class="Products-in-stock">En stock</span></p>

                        </div>

                        <div class="head-medial-text padT10">

                            <p>Código de producto  <span class="Products-in-stock"> #4657</span></p>

                        </div>

                        <div class="product-detail-btn  padT15">

                            <div class="form-row col-sm-4 col-xs-12 country-boxs billing-box select ">

                                <div class="row">

                                    <p>

                                        <label for="billing_state29">Material</label>

                                        <select class="billing_state28" id="billing_state29" name="billing_state">

                                            <option value="">Seleccionar el color</option>

                                            <option value="AP">Rojo</option>

                                            <option value="AR">Azul</option>

                                            <option value="AS">Verde</option>

                                            <option value="BR">Amarillo</option>

                                            <option value="CT">Negro</option>

                                        </select>

                                    </p>

                                </div>

                            </div>

                            <div class="form-row col-sm-4 country-boxs billing-box select down ">

                                <div class="row ">

                                    <p>

                                        <label for="billing_state31">Marcas</label>

                                        <select class="billg_state" id="billing_state31" name="billing_state">

                                            <option value="">Seleccionar marca</option>

                                            <option value="AP">AAA</option>

                                            <option value="AR">BBB</option>

                                            <option value="AS">CCC</option>

                                            <option value="BR">DDD</option>

                                        </select>

                                    </p>

                                </div>

                            </div>

                        </div>

                        <div class="col-md-5 col-sm-5 row">

                            <div class="order-data box order-table ">

                                <div class="order-table-cell order-text product-input-type">

                                    <input type="number" value="01" class="qty" name="qty1">													

                                </div>

                            </div>

                        </div>

                        <div class="col-md-7 col-sm-5 product-box-btm-blog row">

                            <a href="" class="itg-button light pro btn left marT">Añadir a la cesta</a>

                        </div>

                    </div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 padB100 pro-detals-post">

                    <div class="row">

                        <div class="tab table-box sec1">

                            <button class="tablinks active tap-border" data-id="descript">Producto</button>

                            <button class="tablinks tap-border" data-id="sizeGuide">Servicio</button>

                            <button class="tablinks tap-border" data-id="assTags">Etiquetas asignadas</button>

                            <button class="tablinks tap-border" data-id="reviews">Comentarios</button>

                        </div>

                        <div id="descript" class="tabcontent" style="display: block;">

                            <div class="row">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="Profile setting box text border padT20 padB20">

                                        <p>Descripción del producto. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu en elit.</p>

                                        

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div id="sizeGuide" class="tabcontent" style="display: none;">

                            <div class="row">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="Profile setting box text border padT20 padB20">

                                        <p> Descripción del servicio. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu en elit.</p>

                                        

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div id="assTags" class="tabcontent" style="display: none;">

                            <div class="row">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="Profile setting box text border padT30 padB5">

                                        <ul class="tags">

                                            <li><a href="#">aaa</a></li>

                                            <li><a href="#">bbb</a></li>

                                            <li><a href="#">ccc</a></li>

                                            <li><a href="#">ddd</a></li>

                                            <li><a href="#">eee</a></li>

                                            <li><a href="#">fff</a></li>

                                            <li><a href="#">ggg</a></li>

                                            <li><a href="#">hhh</a></li>

                                            <li><a href="#">iii</a></li>

                                            <li><a href="#">jjj</a></li>

                                            <li><a href="#">kkk</a></li>

                                            <li><a href="#">mmm</a></li>

                                        </ul>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div id="reviews" class="tabcontent" style="display: none;">

                            <div class="row">

                                <div class="Reviews-box-blog">

                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                        <div class="Profile setting box text border padT30 padB10">

                                            <div class="comment-section pad-top50 pad-bottom30">

                                                <div class="reviews-container">

                                                    <!--//==Comment List Item Start==//-->

                                                    <div class="comment-box marB30">

                                                        <div class="row">

                                                            <div class="col-md-2 col-sm-3 col-xs-4">

                                                                <figure>

                                                                    <img src="assets/img/blog/comment-1.jpg" alt="" class="img-circle">

                                                                </figure>

                                                            </div>

                                                            <div class="col-md-10 col-sm-9 col-xs-8">

                                                                <h4>Lorem Ipsum | <span>20 febrero - 2020</span></h4>

                                                                <p>Esta es la versión de Lorem Ipsum de Photoshop. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu en elit.</p>

                                                                <p><a href="#">Respuesta</a></p>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="comment-box marB30">

                                                        <div class="row">

                                                            <div class="col-md-2 col-sm-3 col-xs-4">

                                                                <figure>

                                                                    <img src="assets/img/blog/comment-2.jpg" alt="" class="img-circle">

                                                                </figure>

                                                            </div>

                                                            <div class="col-md-10 col-sm-9 col-xs-8">

                                                                <h4>Lorem Ipsum | <span>20 febrero - 2020</span></h4>

                                                                <p>Esta es la versión de Lorem Ipsum de Photoshop. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu en elit.</p>

                                                                <p><a href="#">Respuesta</a></p>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="comment-box marB30">

                                                        <div class="row">

                                                            <div class="col-md-2 col-sm-3 col-xs-4">

                                                                <figure>

                                                                    <img src="assets/img/blog/comment-1.jpg" alt="" class="img-circle">

                                                                </figure>

                                                            </div>

                                                            <div class="col-md-10 col-sm-9 col-xs-8">

                                                                <h4>Lorem Ipsum | <span>20 febrero - 2020</span></h4>

                                                                <p>Esta es la versión de Lorem Ipsum de Photoshop. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu en elit.</p>

                                                                <p><a href="#">Respuesta</a></p>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="comment-box marB30">

                                                        <div class="row">

                                                            <div class="col-md-2 col-sm-3 col-xs-4">

                                                                <figure>

                                                                    <img src="assets/img/blog/comment-2.jpg" alt="" class="img-circle">

                                                                </figure>

                                                            </div>

                                                            <div class="col-md-10 col-sm-9 col-xs-8">

                                                                <h4>Lorem Ipsum | <span>20 febrero - 2020</span></h4>

                                                                <p>Esta es la versión de Lorem Ipsum de Photoshop. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu en elit.</p>

                                                                <p><a href="#">Respuesta</a></p>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <!--//========comment section End=========//-->

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <!--//================Product section start==============//-->

                <div class="clear"></div>

                <!--//================product section end==============//-->

            </div>

        </section>

        <!--//================order page end==============//-->

        <div class="clear"></div>

<?php

include "footer.php";

?>