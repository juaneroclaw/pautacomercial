<?php

ob_start();

session_start();

error_reporting(0);

include "header.php";

?>

        <div class="clear"></div>

        <!--//================Bredcrumb starts==============//-->

        <section>

            <div class="bredcrumb-section padTB100 positionR">

                <div class="container">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="page-head">

                                <div class="page-header-heading">

                                    <h3 class="theme-color">Sobre nosotros</h3>

                                </div>

                                <div class="breadcrumb-box">

                                    <ul class="breadcrumb colorW marB0">

                                        <li>

                                            <a href="index.html">Casa</a>

                                        </li>

                                        <li class="active">Sobre nosotros</li>

                                    </ul>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <!--//================Bredcrumb end==============//-->

        <div class="clear"></div>

        <!--//================ About start ==============//-->

        <section class="padT100">

            <!--- Theme heading start-->

            <div class="theme-heading marB50 positionR">

                <div class="container">

                    <div class="row">

                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">

                            <h1>Sobre nosotros</h1>

                            <div class="heading-lines"><span class="saf-boxs"></span></div>

                            <p>Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web.

                            </p>

                        </div>

                    </div>

                </div>

            </div>

            <!--- Theme heading end-->	

            <div class="container">

                <div class="row">

                    <div class="about-us">

                        <div class="col-md-6 col-sm-12 col-xs-12 pull-right">

                            <figure class="marB30">

                                <img src="assets/img/all/about-1.jpg" alt=""/>

                            </figure>

                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12 pull-left">

                            <h3 class="marB10">Aumente los beneficios con nuevos paquetes</h3>

                            <div>

                                <p>Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web.Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web.Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web. 

                                </p>

                            </div>

                            <a href="register.html" class="itg-button light marB30">Lee mas</a>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <!--//================ About end ==============//-->

        <div class="clear"></div>

        <!--//================ Our Team start ==============//--><!--//================ Our Team end ==============//-->

<div class="clear"></div>

        <!--//================ Our Customer start ==============//-->

        <section class="padTB100 customer-section" >

            <!--- Theme heading start-->

            <div class="theme-heading background marB100 positionR">

                <div class="container">

                    <div class="row">

                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">

                            <h1>Clientes felices</h1>

                            <div class="heading-lines"><span class="saf-boxs"></span></div>

                            <p>Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web.

                            </p>

                        </div>

                    </div>

                </div>

            </div>

            <!--- Theme heading end-->	

            <div class="container">

                <div class="row">

                    <div id="customer-slider" class="owl-carousel owl-theme slider positionR">

                        <div class="item">

                            <div class="col-md-12">

                                <div class="customer-box">

                                    <blockquote>Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web.

                                    </blockquote>

                                    <div class="customer-detail">

                                        <div class="customer-img">

                                            <figure>

                                                <img src="assets/img/all/5.jpg" alt=""/>

                                            </figure>

                                        </div>

                                        <div class="caption">

                                            <h3>Pellentesque cursus</h3>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="item">

                            <div class="col-md-12">

                                <div class="customer-box">

                                    <blockquote>Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web.

                                    </blockquote>

                                    <div class="customer-detail">

                                        <div class="customer-img">

                                            <figure>

                                                <img src="assets/img/all/6.jpg" alt=""/>

                                            </figure>

                                        </div>

                                        <div class="caption">

                                            <h3>Pellentesque cursus</h3>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="item">

                            <div class="col-md-12">

                                <div class="customer-box">

                                    <blockquote>Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web.

                                    </blockquote>

                                    <div class="customer-detail">

                                        <div class="customer-img">

                                            <figure>

                                                <img src="assets/img/all/5.jpg" alt=""/>

                                            </figure>

                                        </div>

                                        <div class="caption">

                                            <h3>Pellentesque cursus</h3>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="item">

                            <div class="col-md-12">

                                <div class="customer-box">

                                    <blockquote>Aumente los beneficios con nuevos paquetes publicitarios para sus anunciantes que incluyen campañas exclusivas en la web.

                                    </blockquote>

                                    <div class="customer-detail">

                                        <div class="customer-img">

                                            <figure>

                                                <img src="assets/img/all/6.jpg" alt=""/>

                                            </figure>

                                        </div>

                                        <div class="caption">

                                            <h3>Pellentesque cursus</h3>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <!--//================ Our Customer end ==============//-->

        <div class="clear"></div>

        <div class="clear"></div>

        <!--//================ Our Partner start ==============//-->

        <div class="partner-box">

            <div class="container">

                <div class="row">

                    <div id="partner-slider" class="owl-carousel owl-theme owl-loaded owl-drag">

                        <div class="col-xs-12">

                            <div class="item">

                                <figure>

                                    <img src="assets/img/partner/4.png" alt="">

                                </figure>

                            </div>

                        </div>

                        <div class="col-xs-12">

                            <div class="item">

                                <figure>

                                    <img src="assets/img/partner/5.png" alt="">

                                </figure>

                            </div>

                        </div>

                        <div class="col-xs-12">

                            <div class="item">

                                <figure>

                                    <img src="assets/img/partner/6.png" alt="">

                                </figure>

                            </div>

                        </div>

                        <div class="col-xs-12">

                            <div class="item">

                                <figure>

                                    <img src="assets/img/partner/7.png" alt="">

                                </figure>

                            </div>

                        </div>

                        <div class="col-xs-12">

                            <div class="item">

                                <figure>

                                    <img src="assets/img/partner/8.png" alt="">

                                </figure>

                            </div>

                        </div>

                        <div class="col-xs-12">

                            <div class="item">

                                <figure>

                                    <img src="assets/img/partner/6.png" alt="">

                                </figure>

                            </div>

                        </div>

                        <div class="col-xs-12">

                            <div class="item">

                                <figure>

                                    <img src="assets/img/partner/7.png" alt="">

                                </figure>

                            </div>

                        </div>

                        <div class="col-xs-12">

                            <div class="item">

                                <figure> 

                                    <img src="assets/img/partner/8.png" alt="">

                                </figure>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!--//================ Our Partner end ==============//-->

        <div class="clear"></div>

<?php

include "footer.php";

?>