<?php
//Include database configuration file
include('config.php');
?>
<!DOCTYPE html>
<html lang="zxx">
    <!--
        **********************************************************************************************************
        Copyright (c) 2017 It-Geeks
        ********************************************************************************************************** 
        -->
 <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
        <![endif]-->
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Product Catalog" />
        <meta name="author" content="itgeeksin.com" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Product Catalog</title>
        <!-- Bootstrap -->
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
        <!-- Master Css -->
        <link href="main.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<script src="jquery.min.js"></script>
  </head>
   
<body>
  <!--//================Header start==============//-->  
        <header class="positionR">
            <div id="main-menu" class="wa-main-menu">
                <!-- Menu -->
                <div class="wathemes-menu relative">
                    <!-- navbar -->
                    <div class="navbar navbar-default black-bg mar0" role="navigation">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    <div class="row">
                                        <div class="navbar-header">
                                            <!-- Button For Responsive toggle -->
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="sr-only">Toggle navigation</span> 
                                            <span class="icon-bar"></span> 
                                            <span class="icon-bar"></span> 
                                            <span class="icon-bar"></span>
                                            </button> 
                                            <!-- Logo -->
                                            <a class="navbar-brand box" href="index.html">
                                            <img class="logo-nav" alt="" width="80px" height="120px" src="assets/img/commercia.jpg" />
                                            </a>
                                        </div>
                                        <!-- Navbar Collapse -->
                                        <div class="navbar-collapse collapse">
                                            <!-- nav -->
                                            <ul class="nav navbar-nav">
                                                <li><a href="index.php">Home</a></li>
												<li><a href="about-us.php">AboutUs</a></li> 
                                                <li><a href="search.php">Search</a></li>
                                                <li><a href="contact-us.php">ContactUs</a></li>											                                      
                                               
                                            </ul>
                                        </div>
                                        <!-- navbar-collapse -->
                                    </div>
                                </div>
                                <!-- col-md-12 -->
                                <!-- col-md-3 -->
                                <div class="col-md-4 col-sm-4">
							  <div class="nav-seach-box">
                                        <input type="text" name="search" class="search_terms" placeholder="Search">
                                        <button class="button-style" type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="navigation-icon custom-drop">
                                        <ul class="social-icon top-bar-icon">
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- col-md-3 -->
                            </div>
                            <!-- row -->
                        </div>
                        <!-- container -->
                    </div>
                    <!-- navbar -->
                </div>
                <!--  Menu -->
            </div>
        </header>
        <!--//================Header end==============//-->
        <div class="clear"></div>
      
	  
	               <?php
		  $result = mysqli_query($con, "select * from `products`;");
			while($row=mysqli_fetch_array($result)){
  $file=$row['products_file'];
  $filename = $row['products_file'];
  header('Content-type: application/pdf');
  header('Content-Disposition: inline; filename="' . $filename . '"');
  header('Content-Transfer-Encoding: binary');
  header('Accept-Ranges: bytes');
  ?>
  
			
			<div class="container" >
                    <div class="row ">
                           <div class="col-md-8 col-sm-8 col-xs-12 " >
						<?php @readfile($file);?>
                </div>
	  </div>
	  </div>
	  
    <div class="clear"></div>
        <!--//================Footer start==============//-->
        <footer class="main_footer">
            <div class="container">
                <div class="footer-box padT50 padB30">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">About us</h3>
                                <p> 
                                    Lorem Ipsum is simply dummy text of the simply dummy printing typese tting
                                    and industry
                                </p>
                                <ul class="footer-icon-box">
                                    <li> 
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <span>hello@gmail.com</span>
                                    </li>
                                    <li> 
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <span>P: 3333 222 1111</span>
                                    </li>
                                    <li> 
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span> 99 Barnard St States - GA 22222 </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">Customer care</h3>
                                <ul class="pad0">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Customer Service</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Site Map</a></li>
                                    <li><a href="#">Advanced Search</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">Your account</h3>
                                <ul class="pad0">
                                    <li><a href="#">Sign In</a></li>
                                    <li><a href="#">View Cart</a></li>
                                    <li><a href="#">My Wishlist</a></li>
                                    <li><a href="#">Track My Order</a></li>
                                    <li><a href="#">Forum Support</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec foot-img-box marB30">
                                <h3 class="colorW marB20">Gallery</h3>
                                <ul class="pad0">
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(1).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(2).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(3).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(4).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(5).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(6).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(7).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(8).jpg" alt=""/></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new-letter-section marT50 marB50">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-12 new-letter-box-text">
                            <h3>Our Newsletter</h3>
                            <p>Contrary to popular belief, Lorem Ipsum</p>
                        </div>
                        <div class="col-md-4 col-sm-4 col-12">
                            <div class="nav-seach-box">
                                <input type="text" name="search" class="search_terms" placeholder="email@example.com">
                                <button class="button-style" type="submit" value=""><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="footer-bottom-icons-section">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="foot-sec box1">
                                        <ul class="social-icon">
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-footer padTB20 bagB">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 colorW">
                            <p><a href="http://www.itgeeksin.com/"><span class="theme-color">� CopyRights Anit Solutions</span></a></p>
                        </div>
                  
                    </div>
                </div>
            </div>
        </footer>
        <!--//================Footer end==============//-->
        <!--//================Quick view Start ==============//--> 
        <div class="quick-vive-popap">
            <div class="container">
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog modal-lg" >
                        <div class="Quick-view-popup modal-content text-left">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="col-md-12 col-xs-12 popap-open-box">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-12 marB30">
                                                <figure>
                                                    <img src="assets/img/all/product-detail.jpg" alt="">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 responsive-top">
                                        <h3 class="theme-headdings text-left  product-detail-title s"><a href="#">Your Product Title</a></h3>
                                        <div class="star-box-section left product-det text-left padT15">
                                            <ul>
                                                <li>
                                                    <p>	<i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="star-code fa fa-star-half-o" aria-hidden="true"></i>
                                                    </p>
                                                </li>
                                                <li class="border-left-site">(3) Reviews</li>
                                                <li class="border-left-site">Add a Review</li>
                                            </ul>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="per-box texy-left marT15">
                                            <p>Morbi mollis vestibulum sollicitudin. in eros a justo facilisis rutrum. Aenean id ullamcorper libero. tempor et purus vitae, consectetur varius nunc.</p>
                                        </div>
                                        <div class="product-detail-btn  padT15">
                                            <div class="form-row col-sm-5 col-xs-12 country-boxs billing-box select ">
                                                <div class="row">
                                                    <p>
                                                        <label for="billing_state28">Material</label>
                                                        <select class="billing_state28" id="billing_state28" name="billing_state">
                                                            <option value="">Select Color</option>
                                                            <option value="AP">Red</option>
                                                            <option value="AR">Blue</option>
                                                            <option value="AS">Green</option>
                                                            <option value="BR">Yellow</option>
                                                            <option value="CT">Black</option>
                                                        </select>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="form-row col-sm-5 country-boxs billing-box select down ">
                                                <div class="row ">
                                                    <p>
                                                        <label for="billing_state27">Brands</label>
                                                        <select class="billg_state" id="billing_state27" name="billing_state">
                                                            <option value="">Select Brand</option>
                                                            <option value="AP">PTM</option>
                                                            <option value="AR">Clara</option>
                                                            <option value="AS">Ziveg</option>
                                                            <option value="BR">Clara</option>
                                                        </select>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-7 row">
                                            <div class="order-data box order-table ">
                                                <div class="order-table-cell order-text product-input-type">
                                                    <input type="number" value="01" class="qty" name="qty1">													
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 col-sm-5 product-box-btm-blog row">
                                            <a href="" class="itg-button light pro btn left marT">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//================Quick view End ==============//-->		
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/plugin/megamenu/js/hover-dropdown-menu.js"></script>
        <script src="assets/plugin/megamenu/js/jquery.hover-dropdown-menu-addon.js"></script>
        <script src="assets/plugin/owl-carousel/js/owl.carousel.min.js"></script>
        <script src="assets/plugin/acordian/js/jquery-ui.js"></script>
        <script src="assets/plugin/fancyBox/js/jquery.fancybox.pack.js"></script> 		
        <script src="assets/plugin/fancyBox/js/jquery.fancybox-media.js"></script>
        <script src="assets/plugin/vertical-slider/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="assets/plugin/counter/js/jquery.countTo.js"></script> 
        <script type="text/javascript" src="assets/plugin/counter/js/jquery.appear.js"></script>
        <script src="assets/js/main.js"></script>

</body>
</html>
