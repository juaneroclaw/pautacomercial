<!--//================Footer start==============//-->

        <footer class="main_footer">

            <div class="container">

                <div class="footer-box padT50 padB30">

                    <div class="row">

                        <div class="col-md-3 col-sm-6 col-xs-12">

                            <div class="foot-sec marB30">

                              <h3 class="colorW marB20">Pauta Comercial</h3>

                                <p> 

                                    <li><a href="about-us.php">Acerca de Pauta Comercial.com</a></li>

                                </p>

                                <ul class="footer-icon-box">

                                    <li> 

                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>

                                        <span><a href="mailto:info@pautacomercial.com" target="_blank">info@pautacomercial.com</a></span>

                                    </li>

                                    <li> 

                                        <i class="fa fa-phone" aria-hidden="true"></i>

                                        <span>P:+5411 5982 0144</span>

                                    </li>

                                    <li> 

                                        <i class="fa fa-map-marker" aria-hidden="true"></i>

                                        <span> Buenos Aires, Argentina</span>

                                    </li>
                                    
                                    <li><a href="index.php">Mapa del sitio</a></li>

                              </ul>

                            </div>

                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">

                            <div class="foot-sec marB30">

                                <h3 class="colorW marB20">Ventas</h3>

                                <ul class="pad0">

                                    <li><a href="contact-us.php">Ser Representante de Ventas</a></li>
                                    
                                    <li><a href="contact-us.php">Bonificaciones</a></li>

                                    <li><a href="contact-us.php">Contacto</a></li>

                                    
                                </ul>

                            </div>

                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">

                            <div class="foot-sec marB30">

                                <h3 class="colorW marB20">Servicios</h3>

                                <ul class="pad0">

                                    <li><a href="contact-us.php">Registro</a></li>

                                    <li><a href="contact-us.php">Consultas</a></li>
                 
                                   <li><a href="medios.php">Búsqueda Avanzada</a></li>

                                </ul>

                            </div>

                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">

                            <div class="foot-sec foot-img-box marB30">

                                <h3 class="colorW marB20">Categorías</h3>

                                <ul class="pad0">

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(1).png" alt="A+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(2).png" alt="B+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(3).png" alt="C+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(4).png" alt="D+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(5).png" alt="E+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(6).png" alt="F+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(7).png" alt="G+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(8).png" alt="H+"/></a></li>
                                    
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(5).png" alt="I+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(6).png" alt="J+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(7).png" alt="K+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(8).png" alt="L+"/></a></li>
                                    
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(5).png" alt="M+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(6).png" alt="N+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(7).png" alt="O+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(8).png" alt="P+"/></a></li>
                                    
                                     <li><a href="#"><img src="assets/img/all/foot-001%20(5).png" alt="Q+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(6).png" alt="R+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(7).png" alt="S+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(8).png" alt="T+"/></a></li>
                                    
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(5).png" alt="U+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(6).png" alt="V+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(7).png" alt="Y+"/></a></li>

                                    <li><a href="#"><img src="assets/img/all/foot-001%20(8).png" alt="Z+"/></a></li>

                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="new-letter-section marT50 marB50">

                    <div class="row">

                        <div class="col-md-4 col-sm-4 col-12 new-letter-box-text">

                            <h3> Newsletter</h3>

                            <p>Coloque el correo electrónico para suscribirse.</p>

                        </div>

                        <div class="col-md-4 col-sm-4 col-12">

                            <div class="nav-seach-box">

                                <input type="text" name="search" class="search_terms" placeholder="info@pautacomercial.com">

                                <button class="button-style" type="submit" value=""><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                <br>
                                Nunca compartiremos tu dirección de correo electrónico con una tercera persona.
                            </div>

                        </div>

                        <div class="footer-bottom-icons-section">

                            <div class="row">

                                <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="foot-sec box1">

                                        <ul class="social-icon">

                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>

                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>                                            
                                                                                    
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-wechat" aria-hidden="true"></i></a></li>

                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>

                                        </ul>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="bottom-footer padTB20 bagB">

                <div class="container">

                    <div class="row">

                        <div class="col-md-6 col-sm-6 col-xs-12 colorW">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
 <td><div align="left"><a href="http://www.pautacomercial.com"><span class="theme-color">Política de Privacidad</span></a>&nbsp;  |&nbsp; <a href="http://www.pautacomercial.com"><span class="theme-color">Términos de Uso e Información Legal</span></a>&nbsp; | <a href="http://www.pautacomercial.com"><span class="theme-color">Política de Listado de Productos y/o Servicios</span></a></div></td>
                              </tr>
                            </table>
                            <p align="left"><a rel="nofollow" href="https://www.pautacomercial.com">©</a> 2020 Pauta Comercial.com. Todos los derechos reservados.                            </p>

                        </div>

                    </div>

                </div>

            </div>

        </footer>

        <!--//================Footer end==============//--> 	

        <!--//================Quick view Start ==============//--> 

        <div class="quick-vive-popap">

            <div class="container">

                <div class="modal fade" id="myModal">

                    <div class="modal-dialog modal-lg" >

                        <div class="Quick-view-popup modal-content text-left">

                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                            </div>

                            <div class="col-md-12 col-xs-12 popap-open-box">

                                <div class="row">

                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <div class="row">

                                            <div class="col-xs-12 marB30">

                                                <figure>

                                                    <img src="assets/img/all/product-detail.jpg" alt="">

                                                </figure>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12 responsive-top">

                                        <h3 class="theme-headdings text-left  product-detail-title s"><a href="#">El título de tu producto</a></h3>

                                        <div class="star-box-section left product-det text-left padT15">

                                            <ul>

                                                <li>

                                                    <p>	<i class="fa fa-star"></i>

                                                        <i class="fa fa-star"></i>

                                                        <i class="fa fa-star"></i>

                                                        <i class="fa fa-star"></i>

                                                        <i class="star-code fa fa-star-half-o" aria-hidden="true"></i>

                                                    </p>

                                                </li>

                                                <li class="border-left-site">(3) Comentarios</li>

                                                <li class="border-left-site">Agrega una reseña</li>

                                            </ul>

                                        </div>

                                        <div class="clear"></div>

                                        <div class="per-box texy-left marT15">

                                            <p>--o--</p>

                                        </div>

                                        <div class="product-detail-btn  padT15">

                                            <div class="form-row col-sm-5 col-xs-12 country-boxs billing-box select ">

                                                <div class="row">

                                                    <p>

                                                        <label for="billing_state28">Material</label>

                                                        <select class="billing_state28" id="billing_state28" name="billing_state">

                                                            <option value="">Seleccionar el color</option>

                                                            <option value="AP">rojo</option>

                                                            <option value="AR">Azul</option>

                                                            <option value="AS">Verde</option>

                                                            <option value="BR">Amarillo</option>

                                                            <option value="CT">Negro</option>

                                                        </select>

                                                    </p>

                                                </div>

                                            </div>

                                            <div class="form-row col-sm-5 country-boxs billing-box select down ">

                                                <div class="row ">

                                                    <p>

                                                        <label for="billing_state27">Marcas</label>

                                                        <select class="billg_state" id="billing_state27" name="billing_state">

                                                            <option value="">Seleccionar marcas</option>

                                                            <option value="AP">PTM</option>

                                                            <option value="AR">Clara</option>

                                                            <option value="AS">Ziveg</option>

                                                            <option value="BR">Clara</option>

                                                        </select>

                                                    </p>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-6 col-sm-7 row">

                                            <div class="order-data box order-table ">

                                                <div class="order-table-cell order-text product-input-type">

                                                    <input type="number" value="01" class="qty" name="qty1">													

                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-7 col-sm-5 product-box-btm-blog row">

                                            <a href="" class="itg-button light pro btn left marT">Añadir a la cesta</a>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!--//================Quick view End ==============//-->