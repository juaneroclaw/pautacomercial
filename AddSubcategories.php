<?php
//Include database configuration file
include('config.php');
include "header.php";
//Pagination	
$perpage = 15;
if(isset($_GET['page']) & !empty($_GET['page'])){
$curpage = $_GET['page'];
}else{
$curpage = 1;
}
$start = ($curpage * $perpage) - $perpage;
$PageSql = "select * from userregistration";
$pageres = mysqli_query($con, $PageSql); 
$totalres = mysqli_num_rows($pageres);
$endpage = ceil($totalres/$perpage);
$startpage = 1;
$nextpage = $curpage + 1;
$previouspage = $curpage - 1;
// fetch content of a specific userid
$sqlsubcat = "select * from userregistration order by `subcat_id` Asc LIMIT $start, $perpage";
$resultsubcat = mysqli_query($con,$sqlsubcat);
$noofrowssubcat=mysqli_num_rows($resultsubcat);
$a="";
$a1="";
$a2="";
if(isset($_POST['btn_update']) !="")
{
$msg=0;
$subcatid=$_POST['subcat_id'];
$subcategoryname = mysqli_real_escape_string($con, $_POST['subcatname']);
$categoryname = mysqli_real_escape_string($con, $_POST['categoryname']);
$sql="UPDATE subcategory SET subcat_name='$subcategoryname',category_id='$categoryname' WHERE subcat_id='$subcatid'";
$result=mysqli_query($con,$sql);
if($result   <  0)
{
?>
	<script>
	window.location.href = "AddSubcategories.php";
	</script>
<?php
}
else
{
?>
<script>
   window.location.href = "AddSubcategories.php";
</script>
<?php
}
}
if(isset($_POST['btn_delete']) !="")
{
$msg=0;
$subcatid=$_POST['subcat_id'];
$sql="DELETE FROM users WHERE subcat_id='$subcatid'";
$result=mysqli_query($con,$sql);
if($result   <  0)
{
?>
	<script>
	 window.location.href = "AddSubcategories.php";
	</script>
<?php
}
else
{
?>
<script>
    window.location.href = "AddSubcategories.php";
</script>
<?php
}
}	 
?>
<link href="css/sweet.css" rel="stylesheet" >
<link href="css/style.css" rel="stylesheet" >
<meta name="viewport" content="width=device-width, initial-scale=1">
<!---<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--->
<!--- Styling for Table Structure --->
<style>
table {
font-family: arial, sans-serif;
border-collapse: collapse;
width: 100%;
border: 1px solid black;
}
th {
background-color: #000000;
color: white;
}
td, th {
border: 1px solid #dddddd;
text-align: center;
padding: 15px;
}
tr:nth-child(even) {
background-color: #dddddd;
}
tr:hover {background-color: #f5f5f5;}
</style>
<script src="jquery.min.js"></script> 
<div class="clear"></div>
<!--//================Bredcrumb starts==============//-->
<section>
<div class="bredcrumb-section padTB100 positionR">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="page-head">                              
	<div class="page-header-heading">
		<h1 style="padding-left:500px;" class="theme-color">Panel de administrador</h1>
		<h4 style="padding-left:550px;" class="theme-color">Agregar productos</h4>
	</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--//================Bredcrumb end==============//-->
<div class="clear"></div>
<!--//================Register start==============//-->
<!------ Include the above in your HEAD tag ---------->
<div class="container" style="margin:20px;"> 
<div class="row">
<div class="col-md-12">
<button type="button" class="btn btn-info" class="add-project" style="float:right;" name="addofficer" id="addofficer"  data-title="ADD" data-toggle="modal" data-target="#add_officer">Agregar subcategoría</button>
<table class="txp-list" id="alltr" rules="cols" rules="rows" style="margin-top:50px;">
<thead>
<tr>
<th style="text-align:center; padding:20px;" class="txp-list-col-id" scope="col">Subcategoría Id#</th>
<th style="text-align:center" class="txp-list-col-section" scope="col">Nombre de la subcategoría</th>
<th style="text-align:center" class="txp-list-col-section" scope="col">nombre de la categoría</th>
<th style="text-align:center" class="desc txp-list-col-created date" scope="col">Acción</th>
</tr>
</thead>
<tbody id="table_show">
<?php
$i=1;
while ($row = mysqli_fetch_array($resultsubcat)) {
?>
<tr value="show">
<td style="text-align:center; padding-left:30px;padding-right:30px;" class="sorting_1">
<?php echo $i; 
$i++;
?>
</td>
<td style="text-align:center; padding-left:30px;padding-right:30px;" class="sorting_1">
<?php echo $row['subcat_name']; ?>
</td>
<td style="text-align:center; padding-left:30px;padding-right:30px;" class="sorting_1">
<?php
$catid = $row['category_id'];
$sqlcat = "select * from category where category_id='$catid' order by `category_id` Asc";
$resultcat = mysqli_query($con,$sqlcat);
$noofrowscat=mysqli_num_rows($resultcat);
$rowcat = mysqli_fetch_array($resultcat);
?>
<?php echo $rowcat['category_name']; ?>
</td>
<td style="text-align:center; padding-left:30px;padding-right:30px;"><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit<?php echo $row['subcat_id']; ?>" ><span class="glyphicon glyphicon-pencil"></span></button></p>
<p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete<?php echo $row['subcat_id']; ?>" ><span class="glyphicon glyphicon-trash"></span></button></p></td> 				 
</tr>

<!---  Delete Modal Start  --->
<div class="modal fade" id="delete<?php echo $row['subcat_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
<h4 class="modal-title custom_align" id="Heading">Eliminar la Subcategoría</h4>
</div>
<div class="modal-body">
<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Seguro que quieres eliminar este registro?</div>
</div>
<div class="modal-footer ">
<form method="POST" action="AddSubcategories.php">
<input type="hidden" name="subcat_id" value="<?php echo $row['subcat_id']; ?>"/>
<button type="submit" name="btn_delete" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Sí</button>
<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
</form></div>
</div>
<!-- /.modal-content --> 
</div>
<!-- /.modal-dialog --> 
</div>
<!---  Delete Modal End  --->

<!------  Edit Modal Start --->
<div class="modal fade" id="edit<?php echo $row['subcat_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header" style="    color: white;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
background:#5584FF">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
<h4 class="modal-title custom_align" id="Heading">Editar el detalle de la subcategoría</h4>
</div>
<div class="modal-body">
<form method="POST" action="AddSubcategories.php" enctype="multipart/form-data">
<input type="hidden"  value="<?php echo $row['subcat_id']; ?>" name="subcat_id">
<input name="subcatname" placeholder="Nombre de la subcategoría" class="form-control" type="text" value="<?php echo $row['subcat_name']; ?>"><br><br>				 
<select class="form-control" name="categoryname" id="categoryname">
<?php
$catids = $row['category_id'];
$catsql = "select * from category where category_id = '$catids'";
$catresult = mysqli_query($con, $catsql);
$catrow=mysqli_fetch_array($catresult);
?>
<option value="<?php echo $catrow['category_name']; ?>"><?php echo $catrow['category_name']; ?></option>
<?php
$queryselect = "SELECT DISTINCT * From category";
$sql_select = mysqli_query($con, $queryselect);
$numselect = mysqli_num_rows($sql_select);
if($numselect > 0){
while($rowselect=mysqli_fetch_array($sql_select)){  
echo '<option value="'.$rowselect['category_id'].'">'.$rowselect['category_name'].'</option>';
}
}else{
echo '<option value="">Category not available</option>';
}
?>
</select>
</br>
</div>
<div class="modal-footer ">
<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>Cerca</button>
<button type="submit" name="btn_update" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span>Actualizar</button>
</div>
</form>      
</div>
</div>
</div>
<!--- Edit Modal End --->


<?php }
?>         
</tbody>
</table>
<nav aria-label="Page navigation" style="float:center">
  <ul class="pagination">
  <?php if($curpage != $startpage){ ?>
    <li class="page-item">
      <a class="page-link" href="?page=<?php echo $startpage ?>" tabindex="-1" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        <span class="sr-only">First</span>
      </a>
    </li>
    <?php } ?>
    <?php if($curpage >= 2){ ?>
    <li class="page-item"><a class="page-link" href="?page=<?php echo $previouspage ?>"><?php echo $previouspage ?></a></li>
    <?php } ?>
    <li class="page-item active"><a class="page-link" href="?page=<?php echo $curpage ?>"><?php echo $curpage ?></a></li>
    <?php if($curpage != $endpage){ ?>
    <li class="page-item"><a class="page-link" href="?page=<?php echo $nextpage ?>"><?php echo $nextpage ?></a></li>
    <li class="page-item">
      <a class="page-link" href="?page=<?php echo $endpage ?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Last</span>
      </a>
    </li>
    <?php } ?>
  </ul>
</nav>
</div>
</div>
</div>
</div>
</div>
</div>
<!--//================Register end==============//-->
<div class="clear"></div>
<!--//================Footer start==============//-->
<footer class="main_footer">
<div class="container">
<div class="footer-box padT50 padB30">
<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="foot-sec marB30">
	<h3 class="colorW marB20">Sobre nosotros</h3>
	<p> 
		Pauta Comercial es una guía de ediciones digitales
	</p>
	<ul class="footer-icon-box">
		<li> 
			<i class="fa fa-envelope-o" aria-hidden="true"></i>
			<span>info@pautacomercial.com</span>
		</li>
		<li> 
			<i class="fa fa-phone" aria-hidden="true"></i>
			<span>P:+5411 5982 0144</span>
		</li>
		<li> 
			<i class="fa fa-map-marker" aria-hidden="true"></i>
			<span> Buenos Aires, Argentina</span>
		</li>
	</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="foot-sec marB30">
	<h3 class="colorW marB20">Atención al cliente</h3>
	<ul class="pad0">
		<li><a href="#">Sobre nosotros</a></li>
		<li><a href="#">Servicio al cliente</a></li>
		<li><a href="#">Política de privacidad</a></li>
		<li><a href="#">Mapa del sitio</a></li>
		<li><a href="#">Búsqueda Avanzada</a></li>
	</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="foot-sec marB30">
	<h3 class="colorW marB20">Su cuenta</h3>
	<ul class="pad0">
		<li><a href="#">Registro</a></li>
		<li><a href="#">Consultas</a></li>
		<li></li>
		<li></li>
	</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="foot-sec foot-img-box marB30">
	<h3 class="colorW marB20">Galería</h3>
	<ul class="pad0">
		<li><a href="#"><img src="assets/img/all/foot-001%20(1).jpg" alt=""/></a></li>
		<li><a href="#"><img src="assets/img/all/foot-001%20(2).jpg" alt=""/></a></li>
		<li><a href="#"><img src="assets/img/all/foot-001%20(3).jpg" alt=""/></a></li>
		<li><a href="#"><img src="assets/img/all/foot-001%20(4).jpg" alt=""/></a></li>
		<li><a href="#"><img src="assets/img/all/foot-001%20(5).jpg" alt=""/></a></li>
		<li><a href="#"><img src="assets/img/all/foot-001%20(6).jpg" alt=""/></a></li>
		<li><a href="#"><img src="assets/img/all/foot-001%20(7).jpg" alt=""/></a></li>
		<li><a href="#"><img src="assets/img/all/foot-001%20(8).jpg" alt=""/></a></li>
	</ul>
</div>
</div>
</div>
</div>
<div class="new-letter-section marT50 marB50">
<div class="row">
<div class="col-md-4 col-sm-4 col-12 new-letter-box-text">
<h3>Nuestro Newsletter</h3>
<p>Coloque el correo electrónico para suscribirse.</p>
</div>
<div class="col-md-4 col-sm-4 col-12">
<div class="nav-seach-box">
	<input type="text" name="search" class="search_terms" placeholder="info@pautacomercial.com">
	<button class="button-style" type="submit" value=""><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
</div>
</div>
<div class="footer-bottom-icons-section">
<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="foot-sec box1">
			<ul class="social-icon">
				<li><a href="#" class="theme-circle marR10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#" class="theme-circle marR10"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#" class="theme-circle marR10"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				<li><a href="#" class="theme-circle marR10"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href="#" class="theme-circle marR10"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
				<li><a href="#" class="theme-circle marR10"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
<div class="bottom-footer padTB20 bagB">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12 colorW">
<p><a href="http://www.itgeeksin.com/"><span class="theme-color">Política comercial de derechos de autor</span></a></p>
</div>
</div>
</div>
</div>
</footer>
<!--//================Footer end==============//--> 	

<div id="add_officer" class="modal fade" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header login-header">
<button type="button" class="close" data-dismiss="modal">×</button>
<h4 class="modal-title">Añadir detalles de usuario</h4>
</div>
<form action="AddSubcategories.php" method="POST">
<div class="modal-body">
<?php
if(isset($_POST['btn_saveo'])) {
$subcategoryname = mysqli_real_escape_string($con, $_POST['subcatname']);
$categoryname = mysqli_real_escape_string($con, $_POST['categoryname']);
$sqlinsert = "INSERT INTO subcategory(subcat_name,category_id)VALUES('".$subcategoryname."','".$categoryname."')";
if(mysqli_query($con, $sqlinsert)) {
?>
<script>
	window.location.href = "AddSubcategories.php";
</script>
<?php
} else {
?>
<script>
	window.location.href = "AddSubcategories.php";
</script>
<?php
} 	 
}	 
?>				
<input name="subcatname" placeholder="Nombre de la subcategoría" class="form-control" type="text"><br><br>			 
<select class="form-control" name="categoryname" id="categoryname">
<option value="">Categoría...</option>
<?php
echo $queryselect = "SELECT DISTINCT * From category";
$sql_select = mysqli_query($con, $queryselect);
$numselect = mysqli_num_rows($sql_select);
if($numselect > 0){
while($rowselect=mysqli_fetch_array($sql_select)){  
echo '<option value="'.$rowselect['category_id'].'">'.$rowselect['category_name'].'</option>';
}
}else{
echo '<option value="">Category not available</option>';
}
?>
</select>
<br>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>Cerca</button>
<button type="submit" name="btn_saveo" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span>Salvar</button>
</div>
</div>
</form>
</div>
</div>
<!--  End Modal  --->		
<!--//================Quick view End ==============//-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugin/megamenu/js/hover-dropdown-menu.js"></script>
<script src="assets/plugin/megamenu/js/jquery.hover-dropdown-menu-addon.js"></script>
<script src="assets/plugin/owl-carousel/js/owl.carousel.min.js"></script>
<script src="assets/plugin/acordian/js/jquery-ui.js"></script>
<script src="assets/plugin/fancyBox/js/jquery.fancybox.pack.js"></script> 		
<script src="assets/plugin/fancyBox/js/jquery.fancybox-media.js"></script>
<script src="assets/plugin/vertical-slider/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="assets/plugin/counter/js/jquery.countTo.js"></script> 
<script type="text/javascript" src="assets/plugin/counter/js/jquery.appear.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnJzryw5UTYs4q-8UN4FyTwWIHlxMSYXw"></script>
<script src="assets/js/main.js"></script>
</body>
</html>