    <?php
    //Include database configuration file
    include('config.php');
    
   
	

    ?>
<!DOCTYPE html>
<html lang="zxx">
    <!--
        **********************************************************************************************************
        Copyright (c) 2017 It-Geeks
        ********************************************************************************************************** 
        -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
        <![endif]-->
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Responsive html template for Salon and Spa" />
        <meta name="author" content="itgeeksin.com" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Responsive html template for Salon and Spa</title>
        <!-- Bootstrap -->
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
        <!-- Master Css -->
        <link href="main.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		<script src="jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#category').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'category_id='+countryID,
                success:function(html){
                    $('#subcategory').html(html);
                    $('#products').html('<option value="">Select subcategory first</option>'); 
                }
            }); 
        }else{
            $('#subcategory').html('<option value="">Select category first</option>');
            $('#products').html('<option value="">Select subcategory first</option>'); 
        }
    });
    

});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#country').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'countryajax.php',
                data:'countryname='+countryID,
                success:function(html){
				
                    $('#province').html(html);
                  
                }
            }); 
        }else{
            $('#province').html('<option value="">Select Country first</option>');
           
        }
    });
	   $('#province').on('change',function(){
        var stateId = $(this).val();
		
        if(stateId){
            $.ajax({
                type:'POST',
                url:'countryajax.php',
                data:'stateId='+stateId,
                success:function(html){
                    $('#city').html(html);
                  
                }
            }); 
        }else{
            $('#city').html('<option value="">Select Province first</option>');
           
        }
    });
    

});
</script>
    </head>
<body>
        <!--//================preloader  start==============//--> 
        <div class="preloader">
            <div id="loading-center">
               <img class="logo-nav" alt="" src="assets/img/productcatalog.png">
                <div class="loader"></div>
            </div>
        </div>
        <!--//================preloader  end==============//-->  
        <!--//================Header start==============//-->  
        <header class="positionR">
            <div id="main-menu" class="wa-main-menu">
                <!-- Menu -->
                <div class="wathemes-menu relative">
                    <!-- navbar -->
                    <div class="navbar navbar-default black-bg mar0" role="navigation">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    <div class="row">
                                        <div class="navbar-header">
                                            <!-- Button For Responsive toggle -->
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="sr-only">Toggle navigation</span> 
                                            <span class="icon-bar"></span> 
                                            <span class="icon-bar"></span> 
                                            <span class="icon-bar"></span>
                                            </button> 
                                            <!-- Logo -->
                                            <a class="navbar-brand box" href="index.html">
                                            <img class="logo-nav" alt="" style="height:20%;width:30%" src="assets/img/productcatalog.png" />
                                            </a>
                                        </div>
                                        <!-- Navbar Collapse -->
                                        <div class="navbar-collapse collapse">
                                            <!-- nav -->
                                            <ul class="nav navbar-nav">
                                                <li><a href="index.php">Home</a></li>
												<li><a href="about-us.php">AboutUs</a></li> 
                                                <li><a href="search.php">Search</a></li>
                                                <li><a href="blog-style-1.php">Blogs</a></li>
                                                <li><a href="contact-us.php">ContactUs</a></li>											                                      
                                                <li>
                                                    <a href="#">Products<i class="fa fa-angle-down" aria-hidden="true"></i></a> 
                                                    <ul class="dropdown-menu">
                                                        <li><a href="search.php">Search</a></li>
                                                        <li><a href="product-detail.php">Product Detail</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- navbar-collapse -->
                                    </div>
                                </div>
                                <!-- col-md-12 -->
                                <!-- col-md-3 -->
                                <div class="col-md-4 col-sm-4">
                                    <div class="nav-seach-box">
                                        <input type="text" name="search" class="search_terms" placeholder="Search">
                                        <button class="button-style" type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="navigation-icon custom-drop">
                                        <ul class="social-icon top-bar-icon">
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marL10"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- col-md-3 -->
                            </div>
                            <!-- row -->
                        </div>
                        <!-- container -->
                    </div>
                    <!-- navbar -->
                </div>
                <!--  Menu -->
            </div>
        </header>
        <!--//================Header end==============//-->
        <div class="clear"></div>
        <!--//================Bredcrumb starts==============//-->
        <section>
            <div class="bredcrumb-section padTB100 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-head">                              
                                <div class="page-header-heading">
                                    <h1 style="padding-left:500px;" class="theme-color">Admin Panel</h1>
									<h4 style="padding-left:550px;" class="theme-color">Add Products</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Bredcrumb end==============//-->
        <div class="clear"></div>
        <!--//================Register start==============//-->
		
		
		
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
		<div class="row padB100 " style="padding-left:500px;">
          
<div class="container">
<div class="row">
		<div class="col-md-6">
	<!-- Nav tabs --><div class="card">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Add Product</a></li>
		<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Edit Product</a></li>
		<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Delete Product</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home">
	<?php
	//Get all country data
    $result = mysqli_query($con, "SELECT * FROM category WHERE status = 1 ORDER BY category_name ASC");
    //Count total number of rows
	$rowCount=mysqli_num_rows($result);
	if (isset($_POST['category'])) {
		   
		   $category=$_POST['category'];
			$subcategory=$_POST['subcategory'];
			
	//set validation error flag as false
    $error = false;
    
    //check if form is submitted
    if (isset($_POST['addproduct'])) {
        $productname = mysqli_real_escape_string($con, $_POST['productname']);
        $productdes = mysqli_real_escape_string($con, $_POST['productdes']);
		 $country = mysqli_real_escape_string($con, $_POST['country']);
		 $province = mysqli_real_escape_string($con, $_POST['province']);
		 $city = mysqli_real_escape_string($con, $_POST['city']);
		 $neighbourhood = mysqli_real_escape_string($con, $_POST['neighbourhood']);
		 
        $filename = $_FILES['pic']['name'];
        $tmpname = $_FILES['pic']['tmp_name'];
		$filename1 = $_FILES['pdf']['name'];
        $tmpname1 = $_FILES['pdf']['tmp_name'];
        $destination = "uploads/".rand().$filename;
        move_uploaded_file($tmpname,$destination);
		$destination1 = "uploads/".rand().$filename1;
		move_uploaded_file($tmpname1,$destination1);
		
		 if (!$error) {
			
			$sql = "INSERT INTO products(products_name,products_des,products_img,products_file,subcat_id,country,province,city,neighbourhood)VALUES('".$productname."', '".$productdes."', '".$destination."', '".$destination1."', '".$subcategory."', '".$country."', '".$province."', '".$city."', '".$neighbourhood."')";
             if (mysqli_query($con, $sql)) {
                $successmsg = "Product Successfully Added1!";
				?>
           
          <script> 
		  alert("Product Added Successfully");
		  window.location.href="AddProduct.php";</script>
            <?php
            } else {
								?>
           
          <script> 
		  alert("Error in Adding new Product");
		 
            <?php
                $errormsg = "Error in Adding Product...!";
            }
        }
          		
	}

	}
		?>
		
		
		<div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 style="padding-top:50px;" class="theme-headdings">Add Products</h3>
                            </div>
                            <div class="clear"></div>
							<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="Cart-Totals-box marT30">
								
                                    <div class="row">
                                        <div class="col-md-12 billing-box select ">
                                            <select id="category" name="category">
								            <?php
                                             if($rowCount > 0){
                                              while($row=mysqli_fetch_array($result)){  
                                                echo '<option value="'.$row['category_id'].'">'.$row['category_name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Category not available</option>';
                                                             }
                                                             ?>
                                                     </select>
                                        </div><br></br>
										<div class="col-md-12 billing-box select ">
										<select id="subcategory" name="subcategory" >
                                        </select>
                                            
                                        </div>
										<br></br>
									<?php	  
									$countryresult = mysqli_query($con, "SELECT * FROM countries");
    //Count total number of rows
									$nr=mysqli_num_rows($countryresult);
									?>
										<div class="col-md-12 billing-box select ">
										<select id="country" name="country">
										  <?php
                                             if($nr > 0){
                                              while($row=mysqli_fetch_array($countryresult)){  
                                                echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Country not available</option>';
                                                             }
                                                             ?>
                                          
                                        </select>
                                            
                                        </div>
										<br></br>
										<div class="col-md-12 billing-box select ">
									<select id="province" name="province">
									
                                        </select>
										</div>
										<br>
										<br>
										<div class="col-md-12 billing-box select ">
										<select id="city" name="city">
									
                                        </select>
                                            </div>
                                        
										<br></br>
									
										
										
										  <div class="col-md-6 col-sm-6 billing-box select padT20 responsive-top">
                                    			<?php	  
									$countryresult = mysqli_query($con, "SELECT * FROM countries");
    //Count total number of rows
									$nr=mysqli_num_rows($countryresult);
									?>
									
										<select id="neighbourhood" name="neighbourhood">
										  <?php
                                             if($nr > 0){
                                              while($row=mysqli_fetch_array($countryresult)){  
                                                echo '<option value="'.$row['name'].'">'.$row['name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Country not available</option>';
                                                             }
                                                             ?>
                                          
                                        </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 billing-box select padT20 responsive-top">
                                            <input type="text" name="productname" id="billing_postcode" placeholder="Product Name" class="form-controller search_terms">
                                        </div>
                                        <div class="col-md-6 col-sm-6  billing-box select padT20 responsive-top">
                                            <input type="text" name="productdes" id="billing_postcode0" placeholder="Product Description" class="form-controller box1 search_terms">
                                        </div>
										<div style="padding-top:20px;" class="col-md-12 col-sm-12 col-xs-12 marB10 positionR">
										    <label>Add Picture:</label><label><input type="file" name="pic" /></label>
                                            
                                        </div></br>
										<div style="padding-top:20px;" class="col-md-12 col-sm-12 col-xs-12 marB10 positionR">
										    <label>Add PDF FILE:</label>
											<input type="file" name="pdf" />
                                            
                                        </div>
                                    </div>
									
                                </div>
                            </div>
							
                            <div class="col-md-12 mar-bottom-res">
							 <button type="submit" name="addproduct" value="addproduct" class="itg-button light marT20">Add Products</button>
                            </div>
							</form>
							<span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
                <span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
                        </div>
                    </div>
		</div>
		<div role="tabpanel" class="tab-pane" id="profile">
				<?php
			   if (isset($_POST['category'])) {
		   
		   $category=$_POST['category'];
			$subcategory=$_POST['subcategory'];
			
	//set validation error flag as false
    $error = false;
    
    //check if form is submitted
    if (isset($_POST['addproduct'])) {
        $productname = mysqli_real_escape_string($con, $_POST['productname']);
        $productdes = mysqli_real_escape_string($con, $_POST['productdes']);
		 $country = mysqli_real_escape_string($con, $_POST['country']);
		 $province = mysqli_real_escape_string($con, $_POST['province']);
		 $city = mysqli_real_escape_string($con, $_POST['city']);
		 $neighbourhood = mysqli_real_escape_string($con, $_POST['neighbourhood']);
		 
        $filename = $_FILES['pic']['name'];
        $tmpname = $_FILES['pic']['tmp_name'];
		$filename1 = $_FILES['pdf']['name'];
        $tmpname1 = $_FILES['pdf']['tmp_name'];
        $destination = "uploads/".rand().$filename;
        move_uploaded_file($tmpname,$destination);
		$destination1 = "uploads/".rand().$filename1;
		move_uploaded_file($tmpname1,$destination1);
		
		 if (!$error) {
			
			$sql = "INSERT INTO products(products_name,products_des,products_img,products_file,subcat_id,country,province,city,neighbourhood)VALUES('".$productname."', '".$productdes."', '".$destination."', '".$destination1."', '".$subcategory."', '".$country."', '".$province."', '".$city."', '".$neighbourhood."')";
             if (mysqli_query($con, $sql)) {
                $successmsg = "Product Successfully Added1!";
				?>
           
          <script> 
		  alert("Product Added Successfully");
		  window.location.href="AddProduct.php";</script>
            <?php
            } else {
								?>
           
          <script> 
		  alert("Error in Adding new Product");
		 
            <?php
                $errormsg = "Error in Adding Product...!";
            }
        }
          		
	}

	}
		?>
		
		
		<div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 style="padding-top:50px;" class="theme-headdings">Add Products</h3>
                            </div>
                            <div class="clear"></div>
							<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="Cart-Totals-box marT30">
								
                                    <div class="row">
                                        <div class="col-md-12 billing-box select ">
                                            <select id="category" name="category">
								            <?php
                                             if($rowCount > 0){
                                              while($row=mysqli_fetch_array($result)){  
                                                echo '<option value="'.$row['category_id'].'">'.$row['category_name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Category not available</option>';
                                                             }
                                                             ?>
                                                     </select>
                                        </div><br></br>
										<div class="col-md-12 billing-box select ">
										<select id="subcategory" name="subcategory" >
                                        </select>
                                            
                                        </div>
										<br></br>
									<?php	  
									$countryresult = mysqli_query($con, "SELECT * FROM countries");
    //Count total number of rows
									$nr=mysqli_num_rows($countryresult);
									?>
										<div class="col-md-12 billing-box select ">
										<select id="country" name="country">
										  <?php
                                             if($nr > 0){
                                              while($row=mysqli_fetch_array($countryresult)){  
                                                echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Country not available</option>';
                                                             }
                                                             ?>
                                          
                                        </select>
                                            
                                        </div>
										<br></br>
										<div class="col-md-12 billing-box select ">
									<select id="province" name="province">
									
                                        </select>
										</div>
										<br>
										<br>
										<div class="col-md-12 billing-box select ">
										<select id="city" name="city">
									
                                        </select>
                                            </div>
                                        
										<br></br>
									
										
										
										  <div class="col-md-6 col-sm-6 billing-box select padT20 responsive-top">
                                    			<?php	  
									$countryresult = mysqli_query($con, "SELECT * FROM countries");
    //Count total number of rows
									$nr=mysqli_num_rows($countryresult);
									?>
									
										<select id="neighbourhood" name="neighbourhood">
										  <?php
                                             if($nr > 0){
                                              while($row=mysqli_fetch_array($countryresult)){  
                                                echo '<option value="'.$row['name'].'">'.$row['name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Country not available</option>';
                                                             }
                                                             ?>
                                          
                                        </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 billing-box select padT20 responsive-top">
                                            <input type="text" name="productname" id="billing_postcode" placeholder="Product Name" class="form-controller search_terms">
                                        </div>
                                        <div class="col-md-6 col-sm-6  billing-box select padT20 responsive-top">
                                            <input type="text" name="productdes" id="billing_postcode0" placeholder="Product Description" class="form-controller box1 search_terms">
                                        </div>
										<div style="padding-top:20px;" class="col-md-12 col-sm-12 col-xs-12 marB10 positionR">
										    <label>Add Picture:</label><label><input type="file" name="pic" /></label>
                                            
                                        </div></br>
										<div style="padding-top:20px;" class="col-md-12 col-sm-12 col-xs-12 marB10 positionR">
										    <label>Add PDF FILE:</label>
											<input type="file" name="pdf" />
                                            
                                        </div>
                                    </div>
									
                                </div>
                            </div>
							
                            <div class="col-md-12 mar-bottom-res">
							 <button type="submit" name="addproduct" value="addproduct" class="itg-button light marT20">Add Products</button>
                            </div>
							</form>
							<span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
                <span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
                        </div>
                    </div>
		</div>
		<div role="tabpanel" class="tab-pane" id="messages">
		<div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 style="padding-top:50px;" class="theme-headdings">Add Products</h3>
                            </div>
                            <div class="clear"></div>
							<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="Cart-Totals-box marT30">
								
                                    <div class="row">
                                        <div class="col-md-12 billing-box select ">
                                            <select id="category" name="category">
								            <?php
                                             if($rowCount > 0){
                                              while($row=mysqli_fetch_array($result)){  
                                                echo '<option value="'.$row['category_id'].'">'.$row['category_name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Category not available</option>';
                                                             }
                                                             ?>
                                                     </select>
                                        </div><br></br>
										<div class="col-md-12 billing-box select ">
										<select id="subcategory" name="subcategory" >
                                        </select>
                                            
                                        </div>
										<br></br>
									<?php	  
									$countryresult = mysqli_query($con, "SELECT * FROM countries");
    //Count total number of rows
									$nr=mysqli_num_rows($countryresult);
									?>
										<div class="col-md-12 billing-box select ">
										<select id="country" name="country">
										  <?php
                                             if($nr > 0){
                                              while($row=mysqli_fetch_array($countryresult)){  
                                                echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Country not available</option>';
                                                             }
                                                             ?>
                                          
                                        </select>
                                            
                                        </div>
										<br></br>
										<div class="col-md-12 billing-box select ">
									<select id="province" name="province">
									
                                        </select>
										</div>
										<br>
										<br>
										<div class="col-md-12 billing-box select ">
										<select id="city" name="city">
									
                                        </select>
                                            </div>
                                        
										<br></br>
									
										
										
										  <div class="col-md-6 col-sm-6 billing-box select padT20 responsive-top">
                                    			<?php	  
									$countryresult = mysqli_query($con, "SELECT * FROM countries");
    //Count total number of rows
									$nr=mysqli_num_rows($countryresult);
									?>
									
										<select id="neighbourhood" name="neighbourhood">
										  <?php
                                             if($nr > 0){
                                              while($row=mysqli_fetch_array($countryresult)){  
                                                echo '<option value="'.$row['name'].'">'.$row['name'].'</option>';
                                                       }
                                                   }else{
                                                    echo '<option value="">Country not available</option>';
                                                             }
                                                             ?>
                                          
                                        </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 billing-box select padT20 responsive-top">
                                            <input type="text" name="productname" id="billing_postcode" placeholder="Product Name" class="form-controller search_terms">
                                        </div>
                                        <div class="col-md-6 col-sm-6  billing-box select padT20 responsive-top">
                                            <input type="text" name="productdes" id="billing_postcode0" placeholder="Product Description" class="form-controller box1 search_terms">
                                        </div>
										<div style="padding-top:20px;" class="col-md-12 col-sm-12 col-xs-12 marB10 positionR">
										    <label>Add Picture:</label><label><input type="file" name="pic" /></label>
                                            
                                        </div></br>
										<div style="padding-top:20px;" class="col-md-12 col-sm-12 col-xs-12 marB10 positionR">
										    <label>Add PDF FILE:</label>
											<input type="file" name="pdf" />
                                            
                                        </div>
                                    </div>
									
                                </div>
                            </div>
							
                            <div class="col-md-12 mar-bottom-res">
							 <button type="submit" name="addproduct" value="addproduct" class="itg-button light marT20">Add Products</button>
                            </div>
							</form>
							<span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
                <span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
                        </div>
                    </div>
		</div>
	</div>
</div>
</div>
</div>
</div>
          
		</div>
        <!--//================Register end==============//-->
        <div class="clear"></div>
        <!--//================Footer start==============//-->
        <footer class="main_footer">
            <div class="container">
                <div class="footer-box padT50 padB30">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">About us</h3>
                                <p> 
                                    Lorem Ipsum is simply dummy text of the simply dummy printing typese tting
                                    and industry
                                </p>
                                <ul class="footer-icon-box">
                                    <li> 
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <span>hello@gmail.com</span>
                                    </li>
                                    <li> 
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <span>P: 3333 222 1111</span>
                                    </li>
                                    <li> 
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span> 99 Barnard St States - GA 22222 </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">Customer care</h3>
                                <ul class="pad0">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Customer Service</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Site Map</a></li>
                                    <li><a href="#">Advanced Search</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec marB30">
                                <h3 class="colorW marB20">Your account</h3>
                                <ul class="pad0">
                                    <li><a href="#">Sign In</a></li>
                                    <li><a href="#">View Cart</a></li>
                                    <li><a href="#">My Wishlist</a></li>
                                    <li><a href="#">Track My Order</a></li>
                                    <li><a href="#">Forum Support</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="foot-sec foot-img-box marB30">
                                <h3 class="colorW marB20">Gallery</h3>
                                <ul class="pad0">
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(1).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(2).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(3).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(4).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(5).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(6).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(7).jpg" alt=""/></a></li>
                                    <li><a href="#"><img src="assets/img/all/foot-001%20(8).jpg" alt=""/></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new-letter-section marT50 marB50">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-12 new-letter-box-text">
                            <h3>Our Newsletter</h3>
                            <p>Contrary to popular belief, Lorem Ipsum</p>
                        </div>
                        <div class="col-md-4 col-sm-4 col-12">
                            <div class="nav-seach-box">
                                <input type="text" name="search" class="search_terms" placeholder="email@example.com">
                                <button class="button-style" type="submit" value=""><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="footer-bottom-icons-section">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="foot-sec box1">
                                        <ul class="social-icon">
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                            <li><a href="#" class="theme-circle marR10"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-footer padTB20 bagB">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 colorW">
                            <p><a href="http://www.itgeeksin.com/"><span class="theme-color">© IT GEEKS</span></a></p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 colorW">
                            <ul class="bottom-footer-navigation">
                                <li><a href="index.html">Home</a></li>
                                <li><a href="">Pages</a></li>
                                <li><a href="service.html">Services</a></li>
                                <li><a href="gallery-style-1.html">Portfolio</a></li>
                                <li><a href="blog-full-with-sidebar.html">Blog</a></li>
                                <li><a href="contact-us-style-2.html">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--//================Footer end==============//-->	
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/plugin/megamenu/js/hover-dropdown-menu.js"></script>
        <script src="assets/plugin/megamenu/js/jquery.hover-dropdown-menu-addon.js"></script>
        <script src="assets/plugin/owl-carousel/js/owl.carousel.min.js"></script>
        <script src="assets/plugin/acordian/js/jquery-ui.js"></script>
        <script src="assets/plugin/fancyBox/js/jquery.fancybox.pack.js"></script> 		
        <script src="assets/plugin/fancyBox/js/jquery.fancybox-media.js"></script>
        <script src="assets/plugin/vertical-slider/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="assets/plugin/counter/js/jquery.countTo.js"></script> 
        <script type="text/javascript" src="assets/plugin/counter/js/jquery.appear.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>