<?php
  session_start();
    
  include_once 'config.php';
  
  //check if form is submitted
  if (isset($_POST['login'])) {
  
	  $email = $_POST['email'];
	  $password = $_POST['password'];
	  $result = mysqli_query($con, "SELECT * FROM register WHERE email = '" . $email. "' and password = '" . $password . "'");

	  if ($row=mysqli_fetch_array($result)) {
		  $_SESSION['user_id'] = $row['userid'];
		  $_SESSION['user_name'] = $row['username'];
		  $_SESSION['user_email'] = $row['email'];
		   ?>   
		   
          <script> 
		  alert("Admin LoggedIn");
		  window.location.href="AddProduct.php";</script>
            <?php
	  } 
		  else
		  {
			  
			  $errormsg = "Incorrect Email or Password!!!";
		  }
		  $errormsg = "Incorrect Email or Password!!!";
	  }
  ?>
<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="bootstrap.min.css">

		<!-- Website CSS style -->
		<link rel="stylesheet" type="text/css" href="main.css">

		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
<script type="text/javascript">
</script>		
		
<style>
body, html{
     height: 100%;
 	background-repeat: no-repeat;
 	font-family: 'Oxygen', sans-serif;
}

.main{
 	margin-top: 30px;
	margin-bottom:70px;
}

h1.title { 
	font-size: 50px;
	font-family: 'Passion One', cursive; 
	font-weight: 400; 
}

hr{
	width: 10%;
	color: #fff;
}

.form-group{
	margin-bottom: 15px;
}

label{
	margin-bottom: 15px;
}

input,
input::-webkit-input-placeholder {
    font-size: 11px;
    padding-top: 3px;
}

.main-login{
 	background-color: #fff;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	border-style: groove;

}

.main-center{
 	margin-top: 30px;
 	margin: 0 auto;
 	max-width: 330px;
    padding: 40px 40px;

}

.login-button{
	margin-top: 5px;
}

.login-register{
	font-size: 11px;
	text-align: center;
}
</style>
</head>
<body>
	<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Welcome to Content System</h1>
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
				<div id="message" style="color:red"></div>
					<form class="form-horizontal" method="post" id="loginform">

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email" required="Enter email" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password" required="Enter Password"/>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<input type="submit" name="login" value="login" class="itg-button light Register-box-btn"/>
						</div>
						<div class="login-register">
				            <a href="forgetpassword.php">Forgot password?</a>
				         </div>
					</form>
					 <span class="text-danger"><?phpphp if (isset($errormsg)) { echo $errormsg; } ?></span>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	</body>
</html>