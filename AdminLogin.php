<?php
session_start();
include_once 'config.php';
//check if form is submitted
if (isset($_POST['login'])) {
$email = $_POST['email'];
$password = $_POST['password'];
$result = mysqli_query($con, "SELECT * FROM register WHERE email = '" . $email. "' and password = '" . $password . "'");
if ($row=mysqli_fetch_array($result)) {
$_SESSION['user_id'] = $row['userid'];
$_SESSION['first_name'] = $row['firstname'];
$_SESSION['last_name'] = $row['lastname'];
$_SESSION['user_email'] = $row['email'];
$_SESSION['user_date'] = $row['date'];
$_SESSION['user_country'] = $row['country'];
$_SESSION['user_province'] = $row['province'];
$_SESSION['user_city'] = $row['city'];
$_SESSION['user_neighbourhood'] = $row['neighbourhood'];
?>   
<script>  
alert("Admin LoggedIn");
window.location.href="AddProduct.php";</script>
<?php
} 
else
{
  
  $errormsg = "Incorrect Email or Password!!!";
}
$errormsg = "Incorrect Email or Password!!!";
}
?>
<!DOCTYPE html>
<html lang="zxx">
<!--
**********************************************************************************************************
Copyright (c) 2017 It-Geeks
********************************************************************************************************** 
-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--[if IE]>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
<![endif]-->
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Responsive html template for Salon and Spa" />
<meta name="author" content="itgeeksin.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Pauta Comercial</title>
<!-- Bootstrap -->
<!-- Favicon -->
<link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
<link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
<!-- Master Css -->
<link href="main.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$('#category').on('change',function(){
var countryID = $(this).val();
if(countryID){
$.ajax({
	type:'POST',
	url:'ajaxData.php',
	data:'category_id='+countryID,
	success:function(html){
		$('#subcategory').html(html);
		$('#products').html('<option value="">Select subcategory first</option>'); 
	}
}); 
}else{
$('#subcategory').html('<option value="">Select category first</option>');
$('#products').html('<option value="">Select subcategory first</option>'); 
}
});
});
</script>
</head>
<body>
<div class="clear"></div>
<!--//================Bredcrumb starts==============//-->
<section>
<div class="bredcrumb-section padTB100 positionR">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page-head">
					<div class="page-header-heading">
						<h1 style="padding-left:500px;" class="theme-color">Panel de administrador</h1>
						<h4 style="padding-left:550px;" class="theme-color">Login de administrador</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<!--//================Bredcrumb end==============//-->
<div class="clear"></div>
<!--//================Register start==============//-->
<div class="padT100 padB70 register-section">
<div class="container">
	<div class="register-sec-box padT100 padB70">
		<div class="row">
			<div class="col-xs-12">
				<div class="tab text-center marB50">
					<a class="tablinks active" data-id="login">Iniciar sesión</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div id="login" class="tabcontent theme-form"  style="display:block;">
				<div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2 col-xs-offset-0">
					<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 marB30 positionR">
							<input type="text" name="email" placeholder="Email">
							<i class="fa fa-user input" aria-hidden="true"></i>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 marB10 positionR">
							<input type="password" name="password" placeholder="Contraseña">
							<i class="fa fa-lock input" aria-hidden="true"></i>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 marB30 positionR">
							<input id="checkbox1" type="checkbox" name="$0-$2000" value="$0-$2000">
							<label class="radio-label" for="checkbox1">Recuérdame</label>
							
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 marB30 positionR">
						  <button type="submit" name="login" value="Login" class="itg-button light Register-box-btn">Iniciar sesión</button>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 marB30">
							<div class="Register-bottom-icons">
								<p class="mar0">
									<a href="#" class="theme-circle marLR5"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
									<a href="#" class="theme-circle marLR5"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
									<a href="#" class="theme-circle marLR5"><span><i class="fa fa-dribbble" aria-hidden="true"></i></span></a>
								</p>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!--//================Register end==============//-->
<div class="clear"></div>
<!--//================Footer start==============//-->
<footer class="main_footer">
<div class="bottom-footer padTB20 bagB">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12 colorW">
				<p><a href="http://www.itgeeksin.com/"><span class="theme-color">© TI GEEKS</span></a></p>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 colorW">
				<ul class="bottom-footer-navigation">
					<li><a href="index.php">Casa</a></li>
									<li><a href="about-us.php">Sobre nosotros</a></li> 
									<li><a href="medios.php">Buscar</a></li>
									<li><a href="blog-style-1.php">Blogs</a></li>
									<li><a href="contact-us.php">Contáctenos</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
</footer>
<!--//================Footer end==============//-->	
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugin/megamenu/js/hover-dropdown-menu.js"></script>
<script src="assets/plugin/megamenu/js/jquery.hover-dropdown-menu-addon.js"></script>
<script src="assets/plugin/owl-carousel/js/owl.carousel.min.js"></script>
<script src="assets/plugin/acordian/js/jquery-ui.js"></script>
<script src="assets/plugin/fancyBox/js/jquery.fancybox.pack.js"></script> 		
<script src="assets/plugin/fancyBox/js/jquery.fancybox-media.js"></script>
<script src="assets/plugin/vertical-slider/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="assets/plugin/counter/js/jquery.countTo.js"></script> 
<script type="text/javascript" src="assets/plugin/counter/js/jquery.appear.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>