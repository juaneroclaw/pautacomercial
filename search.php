<?php

$check=0;

session_start();

error_reporting(0);

//Include database configuration file

include('config.php');

//Get all country data

$result = mysqli_query($con, "SELECT * FROM category WHERE status = 1 ORDER BY category_name ASC");

//Count total number of rows

$rowCount=mysqli_num_rows($result);

?>



<script src="jquery.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){

$('#country').on('change',function(){

var countryID = $(this).val();

//alert(countryID);

if(countryID){

$.ajax({

	type:'POST',

	url:'ajaxData.php',

	data:'country='+countryID,

success:function(data){

				//alert(data);

				var str = data;

    var fields = data.split('+++');

var one = fields[0];

var two = fields[1];          

			    $('#province').html(one);

					 $('#mainContainer').html(two);  

                }

            }); 

        }else{

            $('#province').html('<option value="">Select Country first</option>'); 

        }

    });

	

$('#province').on('change',function(){

var provinceId = $('#myprovince').val();

//alert(provinceId);

if(provinceId){

$.ajax({

	type:'POST',

	url:'ajaxData.php',

	data:'province='+provinceId,

	success:function(data){

				//alert(data);

				var str = data;

    var fields = data.split('+++');

var one = fields[0];

var two = fields[1];          

			    $('#city').html(one);

					 $('#mainContainer').html(two);  

                }

            }); 

        }else{

            $('#city').html('<option value="">Select Province first</option>'); 

        }

    });

	

$('#city').on('change',function(){

var cityId = $('#mycity').val();

//alert(cityId);

if(cityId){

$.ajax({

	type:'POST',

	url:'ajaxData.php',

	data:'city='+cityId,

	success:function(data){

				//alert(data);

				var str = data;

    var fields = data.split('+++');

var one = fields[0];

var two = fields[1];          

			         $('#neighbourhood').html(one);

					 $('#mainContainer').html(two);  

                }

            }); 

        }else{

            $('#neighbourhood').html('<option value="">Select City first</option>'); 

        }

    });	



$('#neighbourhood').on('change',function(){

var cityId = $('#myneighbourhood').val();

//alert(cityId);

if(cityId){

$.ajax({

	type:'POST',

	url:'ajaxData.php',

	data:'neighbourhood='+cityId,

	success:function(data){

				//alert(data);

				var str = data;

    var fields = data.split('+++');

var one = fields[0];

var two = fields[1];          

			    //$('#category').html(one);

					 $('#mainContainer').html(two);  

                }

            }); 

        }else{

            $('#category').html('<option value="">Select Neighborhood first</option>'); 

        }

    });



	

$('#category').on('change',function(){

var CategoryId = $(this).val();

//alert(CategoryId);

if(CategoryId){

$.ajax({

	type:'POST',

	url:'ajaxData.php',

	data:'category_id='+CategoryId,

		success:function(data){

				//alert(data);

				var str = data;

    var fields = data.split('+++');

var one = fields[0];

var two = fields[1];          

			    $('#subcategory').html(one);

					 $('#mainContainer').html(two);  

                }

            }); 

        }else{

            $('#subcategory').html('<option value="">Select Neighborhood first</option>'); 

        }

    });

	



$('#subcategory').on('change',function(){

var SubCategoryId = $(this).val();

//alert(SubCategoryId);

if(SubCategoryId){

$.ajax({

	type:'POST',

	url:'ajaxData.php',

	data:'subcat_id='+SubCategoryId,

	success:function(data){

				//alert(data);

				var str = data;

    var fields = data.split('+++');

var one = fields[0];

var two = fields[1];          

			    //$('#subcategory').html(one);

					 $('#mainContainer').html(two);  

					 $('#products').html('<option value="">Select subcategory first</option>');

                }

            }); 

        }else{

            $('#mainContainer').html('<option value="">Select category first</option>');

		$('#products').html('<option value="">Select subcategory first</option>');  

        }

    });



});

</script>

<link href="css/sweet.css" rel="stylesheet" >

<link href="css/style.css" rel="stylesheet" >

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<?php

include "header.php";

?>

	<div class="clear"></div>

	<!--//================Bredcrumb starts==============//-->

	  <section>

		<div class="bredcrumb-section padTB100 positionR">

			<div class="container">

				<div class="row">

					<div class="col-md-3 col-md-offset-5 col-sm-4 col-sm-offset-2">

						<div class="page-head">

							<div class="page-header-heading">

								<h1 class="theme-color">Medios</h1>

								

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</section>

	<!--//================Bredcrumb end==============//-->

	<div class="clear"></div>

	<!--//================Filter start==============//-->

	<section class="">

<div class="filter-section box padT100 padB100">

<div class="container" >

<div class="row" style="padding-left:100px; padding-right:100px;">		

<?php

$sql = "select * from `countries`";

$countryresult = mysqli_query($con, $sql);

//Count total number of rows

$nr=mysqli_num_rows($countryresult);

?>

<div class="col-md-3">

<select id="country" name="country" class="form-control" required>

<option value="0">Todos los países</option>

  <?php

	 if($nr > 0){

	  while($row=mysqli_fetch_array($countryresult)){  

		echo '<option value="'.$row['name'].'">'.$row['name'].'</option>';

			   }

		   }else{ 

			echo '<option value="">Country not available</option>';

					 }

					 ?>

</select>   

</div>



<div id="province">

<?php

$countryresult = mysqli_query($con, "select * from `worldmap`");

//Count total number of rows

$nr=mysqli_num_rows($countryresult);

?>

<div class="col-md-3">

<select id="province" class="form-control" name="province" required>

<option value="0">Toda la provincia</option>

 <?php

	 if($nr > 0){

	  while($row=mysqli_fetch_array($countryresult)){  

		echo '<option value="'.$row['province'].'" style="color:black">'.$row['province'].'</option>';

			   }

		   }else{

			echo '<option value="">Province not available</option>';

					 }

					 ?>

</select>

</div>

</div>



<div id="city">

<?php

$countryresult = mysqli_query($con, "select * from `worldmap`");

//Count total number of rows

$nr=mysqli_num_rows($countryresult);

?>

<div class="col-md-3">

<select id="city" class="form-control" name="city" required>

<option value="0">Todas las ciudades</option>

 <?php

	 if($nr > 0){

	  while($row=mysqli_fetch_array($countryresult)){  

		echo '<option value="'.$row['city'].'" style="color:black">'.$row['city'].'</option>';

			   }

		   }else{

			echo '<option value="">Cities not available</option>';

					 }

					 ?>

</select>

</div> 

</div>



<div id="neighbourhood">

<?php	  

$countryresult = mysqli_query($con, "select * from `worldmap`");

//Count total number of rows

$nr=mysqli_num_rows($countryresult);

?>

	<div class="col-md-3">

<select id="neighbourhood" class="form-control" name="neighbourhood" required>

<option value="0">Todos los barrios</option>

	<?php

	 if($nr > 0){

	  while($row=mysqli_fetch_array($countryresult)){  

		echo '<option value="'.$row['city'].'">'.$row['city'].'</option>';

			   }

		   }else{

			echo '<option value="">Neighborhood not available</option>';

					 }

					 ?>								

</select>

	</div>

	</div>

	

	<div class="col-md-12" >

	<br>

	<br>

	</div>







</div>

			

			

			

<div class="row" style="padding-left:100px; padding-right:100px;">

<div class="col-md-12 col-sm-12">

<?php	  

$countryresult = mysqli_query($con, "select * from `category`");

//Count total number of rows

$nr=mysqli_num_rows($countryresult);

?>

<div class="col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-1">

<select id="category" name="category" class="form-control" required>

<option value="0">Toda la categoría</option>

  <?php

	 if($rowCount > 0){

	  while($row=mysqli_fetch_array($result)){  

		echo '<option value="'.$row['category_id'].'">'.$row['category_name'].'</option>';

			   }

		   }else{

			echo '<option value="">Category not available</option>';

					 }

					 ?>

</select>   

</div> 	



<div id="subcat">

<?php	  

$countryresult = mysqli_query($con, "select DISTINCT subcat_name from `subcategory`");

//Count total number of rows

$nr=mysqli_num_rows($countryresult);

?>

<div class="col-md-3">

<select id="subcategory" name="subcategory" class="form-control" required>

<option value="0">Todos SubCategory</option>

<?php

	 if($rowCount > 0){

	  while($row=mysqli_fetch_array($result)){  

		echo '<option value="'.$row['subcat_name'].'">'.$row['subcat_name'].'</option>';

			   }

		   }else{

			echo '<option value="">SubCategory not available</option>';

					 }

					 ?>

</select>   

</div>	



</div>

</div>

</div>

			</div>

			

			<div id="mainContainer" class="container" style="padding-top:80px;">

			

			<div class="container">

			<div class="row padB70">

				<div class="col-md-3 col-md-offset-5 col-sm-8 col-sm-offset-2">

					<h3 class="marB30">Filtrar publicaciones</h3>

				</div>

	  

   <ul>

   <?php

  

   if (isset($_POST['category'])) {

	$check=1;   

	   $category=$_POST['category'];

		$subcategory=$_POST['subcategory'];

		

		$result = mysqli_query($con, "select * from `products` where `subcat_id`='$subcategory'");

		while($row=mysqli_fetch_array($result)){

	

?>

<?php

?>

               <div  style="pading-left:5px; pading-right:5px;" class="col-md-3 col-sm-6 col-xs-12 mar-bottom-res mar-bottom-table">

					<div class="collection-box product-img theme-hover sticker">

					<input type="hidden" name="id" required value="<?php echo $row['product_id']; ?>" class="form-control" />						 

						<figure class="blog-style">

							<img style="min-height: 250px;" width="250" height="250" src="<?php echo $row['products_img']; ?>" alt="">

							<figcaption>

								<a href="viewpdf.php?file=<?php echo $row['products_file'];?>"><i class="fa fa-link" aria-hidden="true"></i></a>

							</figcaption>

						</figure>

						<div class="product-text-sec-box">

							<div class="product-text-sec-icons1">

								<ul>

									<li><a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>

									<li><a href=""><i class="fa fa-heart" aria-hidden="true"></i></a></li>

								</ul>

							</div>

							<div class="product-text-sec-btn1">

								<a href="product-detail.php?file=<?php echo $row['products_file'];?>" class="itg-button light">  <a href="product-detail.php?file=<?php echo $row['products_file'];?>">Ordenar ahora</a>

							</div>

						</div>

						<h4 class="product-lilte-headings"><a href="product-detail.php?file=<?php echo $row['products_file'];?>"><?php echo $row['products_name']; ?></a></h4>

						<p style="width:200px;"><?php echo substr($row['products_des'],1);?><br></p>

					</div>

				</div>

	<!-- Modal -->

<div class="modal fade" id="myModal<?php echo $rows['product_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-dialog">

    <div class="modal-content"> 

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title" id="myModalLabel">Streamango Videos</h4>

        <p><?php echo $rows1['videoname'];?></p>

      </div>

  <?php

  $videoid = $rows['video_id'];

  $sql1 = "select * from videos where video_id = '$videoid'";

  $result1 = mysqli_query($con,$sql1);

  $users1 = mysqli_num_rows($result1); 

  $rows1 = mysqli_fetch_array($result1);

  $myurl='http://stream.monotrixtechnologies.com';

  ?>

<div class="modal-body">

<div class="thumbnail" style="width:100%; height:100%;">

	<video src="<?php echo $rows1['videoname']; ?>" width="100%" height="70%" controlsList="nodownload" controls>

	<source src="<?php echo $rows1['videoname']; ?>" type="video/mp4">

	 Your browser does not support the video tag.

	 </video> 

	<div class="caption" style="width:100%; height:30%;">

	<textarea class="form-control" rows="5" id="comment"><?php echo $myurl; ?>/<?php echo $rows1['videoname'];?></textarea>

	</div>

  </div> 

</div>



    </div>

  </div>

</div> <!-- /#myModal -->			

				

   

<?php	 } }  $_POST['category']=""; ?>

   </ul>

</div>

	 

	 </div>

	 </div>

	</section>

	

	

	

		

   <ul>

   <?php

   if($check!=1){

   ?>

			<div id="content" class="container" <?php if ($rowCount>0){ echo 'style="display:none;"'; } ?> >

			<div class="row padB70">

				<div class="col-md-3 col-md-offset-5 col-sm-4 col-sm-offset-2">

					<h3 class="marB30">Nuestra lista de productos destacados</h3>

				</div>

	  <?php

	  

   

   

   

   if (isset($_POST['category'])) {

	   

	   $category=$_POST['category'];

		$subcategory=$_POST['subcategory'];

		

		$result = mysqli_query($con, "select * from `products`;");

		while($row=mysqli_fetch_array($result)){

?>

   <div style="pading-left:5px; pading-right:5px;" class="col-md-3 col-sm-6 col-xs-12 mar-bottom-res mar-bottom-table">

					<div class="collection-box product-img theme-hover sticker">

					<input type="hidden" name="id" required value="<?php echo $row['product_id']; ?>" class="form-control" />						 

						<figure class="blog-style">

							<img style="min-height: 250px;" width="250" height="250" src="<?php echo $row['products_img']; ?>" alt="">

							<figcaption>

								<a href="viewpdf.php?file=<?php echo $row['products_file'];?>"><i class="fa fa-link" aria-hidden="true"></i></a>

							</figcaption>

						</figure>

						<div class="product-text-sec-box">

							<div class="product-text-sec-icons1">

								<ul>

									<li><a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>

									<li><a href=""><i class="fa fa-heart" aria-hidden="true"></i></a></li>

								</ul>

							</div>

							<div class="product-text-sec-btn1">

								<a href="product-detail.php?file=<?php echo $row['products_file'];?>" class="itg-button light">  <a href="product-detail.php?file=<?php echo $row['products_file'];?>">Ordenar ahora</a>

							</div>

						</div>

						<h4 class="product-lilte-headings"><a href="product-detail.php?file=<?php echo $row['products_file'];?>"><?php echo $row['products_name']; ?></a></h4>

						<p style="width:200px;"><?php echo substr($row['products_des'],1);?><br></p>

					</div>

				</div>

   

<?php	 } } } $_POST['category']=""; ?>

   </ul>

</div>

	 

	 </div>

	

	<!--//================Filter end==============//-->

	<div class="clear"></div>

	<!--//================Related Products start==============//-->

	

	<!--//================Related Products end==============//-->

	<div class="clear"></div>

<?php

include "footer.php";

?>